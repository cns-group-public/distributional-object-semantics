from collections import OrderedDict

import numpy as np

from nets.vgg import vgg_16, vgg_arg_scope
from nets.inception_v1 import inception_v1, inception_v1_arg_scope
from nets.inception_v4 import inception_v4, inception_v4_arg_scope
from nets.resnet_v2 import resnet_v2_50, resnet_arg_scope

imagenet_mean = np.array([123.68, 116.78, 103.94])

NETWORK_CONFIGURATIONS = OrderedDict([
    ('inception_v1', (inception_v1, inception_v1_arg_scope, {'num_classes': 1001}, 'inception_v1.ckpt',
                      'AvgPool_1a_7x7', 0.5, 0.5, 1, 224, 1)),
    ('inception_v4', (inception_v4, inception_v4_arg_scope, {}, 'inception_v4.ckpt',
                      'global_pool', 0.5, 0.5, 1, 299, 1)),
    ('resnet50', (resnet_v2_50,  resnet_arg_scope, {'num_classes': 1001}, 'resnet_v2_50.ckpt',
                  'global_pool', imagenet_mean, 255, 255, 299, 1)),
    ('vgg16', (vgg_16, vgg_arg_scope, {}, 'vgg_16.ckpt',
               'vgg_16/fc7', imagenet_mean, 1, 255, 224, 0)),
])


def normalize(img, mean, std, val_range):
    assert img.min() >= 0 and img.max() <= 1
    img *= val_range
    img -= mean

    if std != 0:
        img /= std

    return img