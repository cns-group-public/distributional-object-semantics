from random import shuffle, seed

import skimage
import tensorflow as tf
import numpy as np
import os
import sys

from skimage.io import imread
from skimage.transform import resize
from tensorflow.contrib import slim
from tensorflow.contrib.framework import assign_from_checkpoint_fn, get_variables_to_restore

# TODO: change inception layers to maxpool instead of logits!!!
from network_configuration import NETWORK_CONFIGURATIONS, normalize


if __name__ == '__main__':

    arg = sys.argv[1]

    if arg == 'cnn':

        """
        Running this branch should yield decent scores on ImageNet.
        """

        IMAGENET_PATH = '/home/timo/datasets/ImageNet'
        BATCH_SIZE = 64
        MAX_SAMPLES = 200

        with open('nets/lsvrc2015_labels.txt', 'r') as f:
            label_ids = f.read().split('\n')
            label_ids = {l: i for i, l in enumerate(label_ids)}

        samples = []
        for dirpath, dirnames, filenames in os.walk(IMAGENET_PATH):
            samples += [('{}/{}'.format(dirpath, filename), label_ids[dirpath[-9:]]) for filename in filenames]

        # x = tf.constant(np.zeros((1, 224, 224, 3), 'float32'))

        seed(123)
        shuffle(samples)

        for model, my_scope, args, checkpoint, layer_name, mean, std, val_range, img_size, label_offset in NETWORK_CONFIGURATIONS.values():

            print('\n{} {} {} {}\n'.format(model.__name__, my_scope.__name__, layer_name, checkpoint))

            with slim.arg_scope(my_scope()):
                inp = tf.placeholder(tf.float32, (BATCH_SIZE, img_size, img_size, 3), name='input')
                out, activations = model(inp, is_training=False, **args)
                # print(activations.keys())

                init_fn = assign_from_checkpoint_fn('models/{}'.format(checkpoint), get_variables_to_restore(),
                                                    ignore_missing_vars=False)

                activation = activations[layer_name]

            sess = tf.Session()
            with sess:
                sess.run(tf.global_variables_initializer())
                init_fn(sess)
                hits = []
                for i in range(min(MAX_SAMPLES, len(samples)) // BATCH_SIZE):
                    images, labels = [], []
                    for img_file, label in samples[i*BATCH_SIZE: (i+1)*BATCH_SIZE]:

                        img = imread(img_file)
                        img = resize(img, (img_size, img_size))
                        if img.ndim == 2:
                            img = np.dstack([img]*3)

                        img = normalize(img, mean, std, val_range)

                        # print(img.shape, img.min(), img.max())

                        images += [img]
                        labels += [label + label_offset]

                    images = np.array(images)
                    labels = np.array(labels)

                    # print(images.shape, images.min(), images.max())
                    cls, q = sess.run([out, activation], {inp: images})


                    # q = q.reshape((1, -1))
                    print(type(q), q.shape)
                    # print(type(cls), cls.shape, )
                    # print(cls.argmax(1))
                    # print(labels)
                    # print()

                    hits += (cls.argmax(1) == labels).tolist()

                    print('accuracy:', np.array(hits).mean(), )


            tf.reset_default_graph()

    elif arg == 'skip-gram':
        from object_context.integration import SkipGram
        from object_context.extract import Extract, TextContextExtractor
        from object_context.scenes import LabelMeScenes
        from object_context.contexts import CoOccurrenceContextSelf

        """
        text8.zip can be obtained from http://mattmahoney.net/dc/text8.zip
        The qualitative samples should make some sense. However, the corpus is too small to obtain very good results.
        """

        a = SkipGram(TextContextExtractor('text8.zip'))
        a.integrate()

        a = SkipGram(Extract(LabelMeScenes(), [CoOccurrenceContextSelf]))
        a.integrate()


