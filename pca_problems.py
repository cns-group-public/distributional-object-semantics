import numpy as np

from configurations import MAX_SCENES
from object_context.contexts import VGG16Context, InceptionV4Context, ResNet50Context, CoCountContextNoself, CoCountContextSelf
from object_context.extract import Extract
from object_context.integration import Averaging
from object_context.scenes import MSCOCOScenes

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA


if __name__ == '__main__':

    scenes = MSCOCOScenes(None, MAX_SCENES)

    configs = [
        # Averaging(Extract(scenes, [VGG16Context, CoOccurrenceContextNoself])),
        Averaging(Extract(scenes, [VGG16Context])),
        Averaging(Extract(scenes, [InceptionV4Context])),
        Averaging(Extract(scenes, [ResNet50Context])),
        Averaging(Extract(scenes, [CoCountContextNoself])),
        Averaging(Extract(scenes, [CoCountContextSelf])),
    ]

    variances = []
    handles = []
    for config in configs:
        config.integrate()
        vecs_arr = np.array(list(config.vectors.values()))

        vecs_pca = PCA()
        vecs_pca.fit_transform(vecs_arr)

        handles += [plt.plot(np.cumsum(vecs_pca.explained_variance_ratio_))[0]]
        print(config.name(), sum(vecs_pca.explained_variance_ratio_[:5]), sum(vecs_pca.explained_variance_ratio_[:10]))

    variances = np.array(variances)

    plt.legend(handles, [c.name() for c in configs])

    plt.ylabel('accumulated explained variances')
    plt.xlabel('number of dimensions')

    plt.axvline(x=10, color='gray', linestyle='dotted')
    plt.savefig('pca_problems.png')
