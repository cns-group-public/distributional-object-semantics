import json
import re
import sys
import os
import numpy as np
from config import SIMLEX_PATH, MEN_PATH
from object_context.evaluate import scene250map


def table_name(experiment_name, latex=True):
    name = experiment_name
    name = name.replace('Averaging_', '')

    for d in ['MS', 'LM', 'RC']:
        name = name.replace(d + '_', '')

    name = name.replace('( ', '(')
    name = name.replace('( ', '(')

    if latex:
        name = name.replace('_', '\_')
    else:
        name = name.replace('_', ' ')

    name = name.strip()

    name = name.replace('W2V', 'W2V-Neg')

    return name


def replace_name(name):
    name = re.sub(r'^(.*)\((.*)\)$', r'\1: \2', name)
    name = re.sub(r'^(.*)_(.*)$', r'EarlyFusion(\2): \1', name)
    return name


if __name__ == '__main__':

    arg = sys.argv[1]

    if arg in {'datasets', 'all'}:
        import json

        from configurations import labelme_scenes, rcnn_scenes, coco_scenes

        scenes_datasets = coco_scenes, rcnn_scenes, labelme_scenes

        lines = []
        for scenes in scenes_datasets:
            scenes.initialize()
            s = scenes.get_scenes()
            n_instances = sum([len(x) for _, x in s])
            lines += [[scenes.name(), str(len(scenes.all_occurring_objects())), str(len(s)), str(n_instances)]]

        # print('\\begin{tabular}{|llll|}')
        # print('\\hline')
        # print('\#classes & \#scenes & \#instances \\')
        # print('\\hline')
        # print('\\hline')
        out = ""
        for l in lines:
            out += ' & '.join(l) + ' \\\\ '

        print(out)
        open('figures/datasets.tex', 'w').write(out)


    if arg in {'benchmarks', 'all'}:
        coco_classes_orig = json.load(open('data/classes.json'))
        coco_classes = [c.replace('remote', 'remote control').replace('mouse', 'computer mouse') for c in coco_classes_orig]

        from configurations import labelme_scenes
        labelme_classes = labelme_scenes.all_occurring_objects()

        benchmarks = [
            ('Scene250', 'data/scene250_averaged.txt', ',', 0, scene250map),
            ('Simlex999', SIMLEX_PATH + '/SimLex-999.txt', '\t', 1, lambda x: x),
            ('MEN', MEN_PATH + '/MEN_dataset_natural_form_full', ' ', 0, lambda x: x),
        ]

        out = ""
        for name, filename, split, offset, word_map in benchmarks:
            text = open(filename).read()
            text_lines = [t for t in text.split('\n')[offset:] if len(t) > 0]

            words = set()
            n_coco_pairs, n_labelme_pairs = 0, 0
            for l in text_lines:
                l = l.split(split)
                if len(l) > 1:
                    words.add(word_map(l[0]))
                    words.add(word_map(l[1]))

                n_coco_pairs += 1 if word_map(l[0]) in coco_classes_orig and word_map(l[1]) in coco_classes_orig else 0
                n_labelme_pairs += 1 if word_map(l[0]) in labelme_classes and word_map(l[1]) in labelme_classes else 0

            out += '{} & {} & {} & {} & {} & {} & {}  \\\\ \n'.format(
                name, len(words), len(text_lines), len(words.intersection(coco_classes)), n_coco_pairs,
                len(words.intersection(labelme_classes)), n_labelme_pairs
            )

        print(out)
        open('figures/benchmarks.tex', 'w').write(out)


    if arg in {'fusion', 'all'}:
        coco = json.load(open('figures/fusion_mscoco_eval_nn.json'))
        coco_words = json.load(open('figures/fusion_mscoco_common_concepts.json'))
        rcnn = json.load(open('figures/fusion_rcnn_eval_nn.json'))
        rcnn_words = json.load(open('figures/fusion_rcnn_common_concepts.json'))



        qualities = ('cos', 'coco_sim_cos', 'coco_rel_cos')

        out = "\\multicolumn{4}{|l|}{\\textbf{MSCOCO}" + " ({} concepts)".format(len(coco_words)) + "} \\\\ \n"
        out += "\\hline \n"
        for k in coco:
            if table_name(k).startswith('Late'): out += "\\hline \n"

            out += replace_name(table_name(k)) + ' & ' + ' & '.join(['{:.3f}'.format(np.mean(coco[k][q])) for q in qualities]) + ' \\\\ \n'

        out += "\\hline \\hline \n"

        out += "\\multicolumn{4}{|l|}{\\textbf{RCNN}" + " ({} concepts)".format(len(rcnn_words)) + "} \\\\\n"
        out += "\\hline \n"
        for k in rcnn:
            if table_name(k).startswith('Late'): out += "\\hline \n"

            out += replace_name(table_name(k)) + ' & ' + ' & '.join(['{:.3f}'.format(np.mean(rcnn[k][q])) for q in qualities]) + ' \\\\\n'

        open('figures/fusion.tex', 'w').write(out)

        print(out)

    if arg in {'fusion_methods', 'all'}:
        coco = json.load(open('figures/fusion_methods_eval_nn.json'))
        qualities = ('cos', 'coco_sim_cos', 'coco_rel_cos')

        out = ""
        for k in coco:
            out += replace_name(table_name(k)) + ' & ' + ' & '.join(['{:.3f}'.format(np.mean(coco[k][q])) for q in qualities]) + ' \\\\ \n'

        open('figures/fusion_methods.tex', 'w').write(out)
        print(out)


    if arg in {'skip_gram', 'all'}:
        scores = json.load(open('figures/skip_gram_eval_nn.json'))
        qualities = ('cos', 'coco_sim_cos', 'coco_rel_cos')

        out = ""
        for k in scores:
            groups = re.match(r'SkipGram_neg(\d+)_ep(\d+)_dim(\d+)', k).groups()
            out += ' & '.join(groups) + ' & ' + ' & '.join(['{:.3f} ({:.3f})'.format(
                np.mean(scores[k][q]), np.std(scores[k][q])
            ) for q in qualities]) + ' \\\\ \n'

        open('figures/skip_gram.tex', 'w').write(out)
        print(out)


    if arg in {'sota', 'all'}:

        dataset_to_name = {'labelme': 'LabelMe', 'mscoco': 'COCO', 'rcnn': 'RCNN'}

        men_lines = open(os.path.join(MEN_PATH, 'MEN_dataset_natural_form_full')).read().split('\n')
        men_pairs = [m.split()[:2] for m in men_lines if len(m) > 0]

        sl_lines = open(os.path.join(SIMLEX_PATH, 'SimLex-999.txt')).read().split('\n')
        sl_pairs = [m.split()[:2] for m in sl_lines[1:] if len(m) > 0]

        s250_lines = open('data/scene250_averaged.txt').read().split('\n')
        s250_pairs = [m.split(',')[:2] for m in s250_lines[0:] if len(m) > 0]
        s250_pairs = [[scene250map(w1), scene250map(w2)] for w1, w2 in s250_pairs]

        coco_classes = json.load(open('data/classes.json'))
        supercategory_lm_classes = json.load(open('data/supercategories_lm.json'))

        for i, datasets in enumerate([['mscoco', 'labelme', 'rcnn']]):
            out = ''
            for i_dataset, dataset in enumerate(datasets):

                scores = json.load(open('figures/sota_{}_eval_nn.json'.format(dataset)))
                occurring_words = json.load(open('figures/sota_{}_common_concepts.json'.format(dataset)))
                qualities = ('cos', 'cos_lm', 'coco_sim_cos', 'coco_rel_cos', 'simlex999_cos', 'MEN_cos')

                intersection_coco = len([1 for w in occurring_words if w in coco_classes])
                intersection_lm = len([1 for w in occurring_words if w in supercategory_lm_classes])
                intersection_s250 = len([1 for w1, w2 in s250_pairs if w1 in occurring_words and w2 in occurring_words])
                intersection_men = len([1 for w1, w2 in men_pairs if w1 in occurring_words and w2 in occurring_words])
                intersection_sl = len([1 for w1, w2 in sl_pairs if w1 in occurring_words and w2 in occurring_words])

                out += "\\textbf{{{}}}".format(dataset_to_name[dataset])
                out += " & {} concepts  & {} concepts & \multicolumn{{2}}{{c|}}{{{} pairs}} & {} pairs & {} pairs \\\\ \n".format(
                    intersection_coco, intersection_lm, intersection_s250, intersection_sl, intersection_men)
                out += "\hline \n"
                for k in scores:
                    if table_name(k) in {'Glove', 'VGG16'}: out += "\\hline \n"
                    out += replace_name(table_name(k)) + ' & '
                    if dataset not in {'mscoco', 'rcnn'}:
                        out += ' & '.join(['{:.3f}'.format(np.mean(scores[k][q])) for q in qualities])
                    else:
                        out += '{:.3f}'.format(np.mean(scores[k]['cos'])) + ' & '
                        out += ' - & '
                        out += ' & '.join(['{:.3f}'.format(np.mean(scores[k][q])) for q in ['coco_sim_cos', 'coco_rel_cos']])
                        out += ' & -'*2
                    out += ' \\\\ \n'

                if i_dataset != len(datasets) - 1:
                    out += "\hline \n"
                    out += "\hline \n"

            open('figures/sota{}.tex'.format(i), 'w').write(out)
            print(out)

        # import ipdb
        # ipdb.set_trace()

    if arg in {'coverage', 'all'}:
        from object_context.concept_vectors import LateFusionNorm
        from configurations import glove_vectors, word2vec_vectors, all_words, coco_scenes, labelme_scenes
        from object_context.concept_vectors import KielaMeanVectors
        from object_context.contexts import VGG16Context, CoOccurrenceContextNoself
        from object_context.extract import Extract
        from object_context.integration import Averaging
        import numpy as np
        import matplotlib.pyplot as plt

        vectors = [
            ('mscoco', Averaging(Extract(coco_scenes, [VGG16Context]))),
            ('labelme', Averaging(Extract(labelme_scenes, [VGG16Context]))),
            ('glove', glove_vectors),
            ('w2v', word2vec_vectors),
            # ('kiela', KielaMeanVectors('/media/resources/KielaMultimodalEmbedding/embeddings/vectors', words=all_words)),
        ]

        words = [set(v.get_words()) for n, v in vectors]
        names = [n for n, _ in vectors]

        all_common = words[0].intersection(*words[1:])

        missing = {}
        for n, w1 in zip(names, words):
            for w in words[0].difference(w1):
                if w in missing:
                    missing[w] += [n]
                else:
                    missing[w] = [n]


        import ipdb
        ipdb.set_trace()

        plt.axis('off')

        for w in all_common:
            x, y = np.random.uniform(1, 2), np.random.uniform(1, 2)
            plt.text(x, y, w)

        for i, (w, l) in enumerate(missing.items()):
            x, y = np.random.uniform(0, 1), np.random.uniform(i, 1)
            plt.text(x, y, w + ' ' + ''.join([x[0] for x in l]))

        plt.savefig('figures/coverage.pdf')






