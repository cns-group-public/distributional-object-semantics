FROM nvidia/cuda:9.0-cudnn7-runtime

RUN pip3 install numpy==1.14.2 scipy=1.0.0 nltk==3.2.5 matplotlib==2.2.0 scikit-learn=0.19.1 scikit-image==0.13.1 pandas==0.22.0
RUN pip3 install tensorflow-gpu==1.6

RUN python3 -c "import nltk; nltk.download('wordnet')"

RUN bash setup_third_party.bash
RUN bash download_datasets.bash

