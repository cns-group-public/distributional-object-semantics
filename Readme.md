
# Installation

The code has been run on Ubuntu 17.10, CUDA 9 and CuDNN v7, but other version will probably also work.

You will need to install quite a few Python 3 packages (pip or pip3 depends on your distribution):
```bash
pip3 install numpy==1.14.2 scipy=1.0.0 nltk==3.2.5 matplotlib==2.2.0 scikit-learn=0.19.1 scikit-image==0.13.1 pandas==0.22.0
```
Tensorflow is also required. See the Tensorflow installation guide on how to setup CUDA and CUDNN. 
Alternatively, there is a CPU-only package of tensorflow called `tensorflow-cpu`.
```bash
pip3 install tensorflow-gpu==1.6
```


We also need to get the wordnet corpus within NLTK:
```python
import nltk
nltk.download('wordnet')
```


## Mask RCNN and Third Party Code
Next, Mask-RCNN needs to incorporated. We use the implementation by Matterport available on 
github. Furthermore, we rely on some pre-trained models from Tensorflow Slim. All of this is downloaded
by running
```bash
bash setup_third_party.bash
```

## Datasets
Finally, before running the experiments, the datasets need to be obtained. The following script assumes 
`/media/resources` to be the dataset folder. If you prefer an other location you need to change the script.
```bash
bash download_datasets.bash
```

# Project Overview

Folders:
`data`: Data required to run experiments and benchmarks.
`figures`: Figures and tables are saved tp this folder.
`object_context`: Internal code for different context models, etc.

* `config.py`: Path definitions of the datasets and cache paths.
* `configurations.py`: Definitions of the experiments
* `diagrams.py`: Render diagrams when the experiments are run.
* `experiments.py`: Script to run the experiments define in `configurations.py`: e.g. `python3 experiments.py mscoco`.
* `network_configuration.py`: Configurations (extraction layer, mean subtraction, ...) for each CNN being used.
* `pca_problems.py`: Script that demonstrates the problems of PCA for dimension reduction (see rebuttal).
* `tables.py`: Build tables when experiments are run.
* `qualitative.py`: Qualitative plots.
* `rcnn_config.py`: Parameters for Mask R-CNN.
* `tables.py`: Script for generating tables (after running `experiments.py`).
* `validate.py`: Verify that the skip-gram extraction and the feature extraction from pre-trained CNNs works properly.
 
# Preparation
Next, the cache needs to be filled. Since this involves heavy computation it might 
take hours to days (depending on your GPU or CPU). Also, unless you have a lot of RAM, it might be necessary to
run each experiment individually.



