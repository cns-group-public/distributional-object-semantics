import re
import json
from itertools import groupby, product

import numpy as np
import sys
from matplotlib import pyplot as plt
# from matplotlib.cm import hot, Paired, tab10, tab20, terrain,
import matplotlib.patheffects as path_effects
from matplotlib import rcParams
from skimage.color import hsv2rgb
from tables import table_name

rcParams['font.sans-serif'] = ['Lato', 'sans-serif']
rcParams['font.weight'] = 'regular'
rcParams['font.size'] = '14'

#
# rcParams['font.family'] = 'sans-serif'
# rcParams['font.sans-serif'] = ['DejaVu Sans']

REPLACEMENTS = {'Averaging': 'Avg'}
MARKERS = "o+x*sdhv8>"


def replace(x):
    if x in REPLACEMENTS:
        return REPLACEMENTS[x]
    else:
        return x


cmap = plt.cm.terrain


all_names = ['{}_{}_{}'.format(intg, dataset, con)
             for con in ['CCs', 'CCn', 'COs', 'COn', 'InceptionV1', 'InceptionV4', 'VGG16', 'ResNet50', 'Dummy']
             for intg in ['Avg', 'SG']
             for dataset in ['MS', 'LM', 'RC']] + ['Glove', 'W2V', 'KielaMax', 'KielaMean']
K = len(all_names)


label_contexts = ['COs', 'COn', 'CCs', 'CCn']
cnn_contexts = ['InceptionV1', 'InceptionV4', 'VGG16', 'ResNet50']
own_datasets = ['MS', 'LM', 'RC']
other = ['Glove', 'W2V', 'KielaMax', 'KielaMean']
methods = ['SkipGram', 'Avg', 'Median']


def cmap(experiment_name):

    if experiment_name in other:
        h_offset = 0.7
        x = other.index(experiment_name ) / len(other)
        return hsv2rgb([[[h_offset + 0.35*x, 0.7, 0.5]]])[0, 0]

    m = re.match(r'^(\w*)_(\w*)_(\w*)$', experiment_name)
    if m:
        a, b, c = m.groups()

        if c == 'Dummy':
            return (0.5, 0.5, 0.5)

        h_offset = 0.0
        if c in label_contexts:
            x = label_contexts.index(c) / len(label_contexts)

        if c in cnn_contexts:
            h_offset = 0.5
            x = cnn_contexts.index(c) / len(cnn_contexts)

        if a == 'SkipGram':
            return hsv2rgb([[[h_offset + 0.9, 0.6, 0.8]]])[0, 0]
        
        return hsv2rgb([[[h_offset + 0.35*x, 0.7, 0.7]]])[0, 0]
    
    if experiment_name in all_names:
        x = all_names.index(experiment_name) / len(all_names)

        # x /= 1.2
        s = (0.5 + 0.5*np.sin(len(all_names) * 0.5 * np.pi*x)) * 0.6
        v = (0.5 + 0.5 * np.cos(len(all_names) * 0.5 * np.pi * x)) * 0.3
        return hsv2rgb([[[np.sin(0.5*x), 0.4 + s - 0.5*np.sin(x), 0.7 + v]]])[0, 0]
    else:
        return 0.2, 0.2, 0.2


def hatch(experiment_name):
    m = re.match(r'^(\w*)_(\w*)_(\w*)$', experiment_name)
    if m:
        a, b, c = m.groups()

        if a == 'Avg':
            return '/'

        if a == 'SkipGram':
            return '.'

        if a == 'Median':
            return '+'

    return ''

# all_names = ['Averaging_RC_VGG16+Averaging_RC_COn',
#              'Averaging_RC_VGG16+Averaging_RC_CCn',
#              'Averaging_RC_VGG16+Averaging_RC_COn+W2V',
#              'Averaging_RC_VGG16+Averaging_RC_CCn+W2V',
#              'Averaging_RC_VGG16+Averaging_RC_COn+Glove',
#              'Averaging_RC_VGG16+Averaging_RC_CCn+Glove',
#              'Averaging_MS_VGG16+Averaging_MS_COn',
#              'Averaging_MS_VGG16+Averaging_MS_CCn',
#              'Averaging_MS_VGG16+Averaging_MS_COn+W2V',
#              'Averaging_MS_VGG16+Averaging_MS_CCn+W2V',
#              'Averaging_MS_VGG16+Averaging_MS_COn+Glove',
#              'Averaging_MS_VGG16+Averaging_MS_CCn+Glove',
#              'Averaging_MS_VGG16+COn',
#              'Averaging_MS_VGG16+COs',
#              'Averaging_MS_InceptionV1+COn',
#              'Averaging_MS_InceptionV1+COs',
#              'Averaging_MS_VGG16+Averaging_MS_COs',
#              'Averaging_MS_VGG16+Averaging_MS_CCs',
#              'Averaging_MS_InceptionV1+Averaging_MS_COn',
#              'Averaging_MS_InceptionV1+Averaging_MS_CCn',
#              'Averaging_MS_InceptionV1+Averaging_MS_COs',
#              'Averaging_MS_InceptionV1+Averaging_MS_CCs',
#              #'Averaging_MS_VGG16+CCn',
#              ] + all_names


def to_normal_name(name):
    match = re.match(r'^(\w*)_([A-Z]*)[0-9]*_(\w*)$', name)

    if match:
        return '_'.join([replace(g) for g in match.groups()])
    else:
        return name




def sizes_plot(qualities, n_contexts, groups, experiments_name, plot_sizes, n_cols, title='aaa'):

    assert len(groups) > 0


    for s in groups:
        handles = []
        handle_names = []
        # fig = pylab.figure(figsize=(9, 6))
        # ax = pylab.plt.subplot(111)

        fig, ax = plt.subplots()
        ax.set_xlim(plot_sizes[0], plot_sizes[-1])
        ax.set_xticks(plot_sizes, )
        ax.set_axisbelow(True)
        ax.yaxis.grid(color='lightgray', linestyle='solid')
        plt.title(title, fontweight='bold')
        plt.setp(ax.spines.values(), color='lightgray')


        # quality based on hit rate
        for i_experiment, experiment_id in enumerate(qualities.keys()):
            color = cmap(to_normal_name(experiment_id))

            score = qualities[experiment_id][s]
            print(experiment_id, score, n_contexts)

            # if type(score) == list:
            #     score = np.mean(score)

            handles += [
                ax.plot(n_contexts, score, color=color, lw=2,
                        marker=MARKERS[i_experiment], label=experiment_id, markersize=9, mew=2, mec=color)[0]]
            ax.set_xscale('log')
            handle_names += [experiment_id]

        ax.set_xlabel('Number of training instances', fontweight='bold', fontsize=12)
        # ax.set_ylabel('P@' + str(h))

        # fig.savefig('figures/%s_train_size_%s.svg' % (experiments_name, s), bbox_inches='tight')
        fig.savefig('figures/%s_train_size_%s.pdf' % (experiments_name, s), bbox_inches='tight')
        plt.clf()

    fig.set_size_inches(5.6, 0.2 * len(handle_names))
    plt.axis('off')
    plt.legend(handles, handle_names, mode='expand', ncol=2, fontsize=10, frameon=False)
    fig.savefig('figures/%s_train_size_legend.pdf' % (experiments_name,), bbox_inches='tight')

    for n_col in n_cols:
        plt.clf()
        plt.axis('off')
        fig.set_size_inches(2.3 * n_col, 0.3 * (len(handle_names) // n_col))
        plt.legend(handles,  [table_name(n, latex=False) for n in handle_names], mode='expand', ncol=n_col, frameon=False)
        plt.savefig('figures/{}_train_size_legend_{}.pdf'.format(experiments_name, n_col), bbox_inches='tight')


def comparison_diagram_multi(qualities, metric_groups, filenames=None, use_last_only=False, format_styles=None,
                             metric_names=None, titles=None, y_lim=None):

    text_fx = [path_effects.Stroke(linewidth=2, foreground='white'), path_effects.Normal()]

    if metric_names is None:
        metric_names = [None for _ in metric_groups]

    for i_metric, metric_group in enumerate(metric_groups):
        print(metric_group)

        all = [(setting, metric, qualities[setting][metric]) for setting in qualities.keys() for metric in metric_group]

        if use_last_only:
            all = [(a, b, c[-1]) for a, b, c in all]

        fig, ax = plt.subplots()

        if y_lim is not None and y_lim[i_metric] is not None:
            assert type(y_lim[i_metric]) == tuple
            assert len(y_lim[i_metric])
            ax.set_ylim(y_lim[i_metric][0], y_lim[i_metric][1])

        ax.yaxis.grid(color='lightgray', linestyle='solid')
        plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')

        grouped = sorted(all, key=lambda x: x[1])
        grouped = groupby(grouped, lambda x: x[1])

        grouped = list([(n, list(g)) for n, g in grouped])

        # fig.set_size_inches(7 * len(metric_group), 6)
        fig.set_size_inches(0.1 + 0.8 * sum([len(g) for n, g in grouped]), 6)
        plt.setp(ax.spines.values(), color='lightgray')

        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['top'].set_visible(False)

        ax.set_ymargin(0.1)
        ax.set_xmargin(0.01)
        ax.set_axisbelow(True)

        fs = 19  # font size

        if titles is not None:
            plt.title(titles[i_metric], fontweight='bold', fontsize=fs + 5)

        format_style = format_styles[i_metric] if format_styles is not None else '{:.3f}'

        current_x = 0
        for i_group, (metric, element_gen) in enumerate(grouped):

            names, _, performances = zip(*list(element_gen))

            normal_names = [to_normal_name(n) for n in names]
            colors = [cmap(n) for n in normal_names]

            print(performances)

            for i, performance in enumerate(performances):
                if len(performance) > 1:
                    for p in performance:
                        plt.hlines(p, current_x + i-0.3, current_x + i+0.3, color=[0.7*c for c in colors[i]])

            #performances = [np.mean(p) for p in performances]
            handles = plt.bar(current_x + np.arange(len(names)), height=[np.mean(p) for p in performances], width=0.85, color=colors,
                              #edgecolor=[[0.7*x for x in c[:3]] for c in colors],
                              linewidth=2)

            for n, handle in zip(normal_names, handles):
                handle.set_facecolor(cmap(n))
                handle.set_edgecolor([c*0.7 for c in cmap(n)[:3]])
                handle.set_hatch(hatch(n))
                #handle.set_alpha(0.5)


            for i, n in enumerate(normal_names):
                plt.text(current_x + i, max(0.02, np.max(performances[i]) + 0.02),
                         format_style.format(np.mean(performances[i])).lstrip('0').replace('-0.', '-.'),
                         fontsize=fs, horizontalalignment='center',
                         path_effects=text_fx, color=(0.35, 0.35, 0.35))

            if metric_names[i_metric] is not None:
                plt.text(current_x + 0.43*len(names), -max([np.mean(p) for p in performances])*0.15, metric_names[i_metric][i_group],
                         fontsize=fs + 5, horizontalalignment='center')

            current_x += len(names) + 0.8

        if filenames is None:
            plt.show()
        else:
            assert len(filenames) == len(metric_groups)
            plt.savefig(filenames[i_metric], bbox_inches='tight')


# handles = []
# handle_names = []


def save_diagrams(eval_sizes, eval_nn, eval_precision, eval_recall, experiments_name, n_contexts, similarities,
                  sim_datasets, plot_sizes):


    # evaluation diagram
    expn = experiments_name
    plot_config = [
        # ('', ('cos', 'euc'), ('cosine', 'euclidean'), 'figures/{}_cluster.pdf'.format(expn), ' {:.1f}', (0, 1)),
        # ('', ('euc',), None, 'figures/{}_cluster_euc.pdf'.format(expn), ' {:.1f}', (0, 1)),
        # ('', ('coco_sim_euc', 'coco_rel_euc'), ('similarity', 'relatedness', (-.1, 0.7)),
        #  'figures/{}_corr_euc.pdf'.format(expn), '{:.3f}', (-.1, 0.7)),
        ('', ('cos',), None, 'figures/{}_cluster_cos.pdf'.format(expn), '{:.3f}', (0, 0.85)),
        ('', ('coco_sim_cos', 'coco_rel_cos'), ('similarity', 'relatedness', (-.1, 0.7)),
         'figures/{}_corr_cos.pdf'.format(expn), '{:.3f}', (-.1, 0.7)),
    ]

    n_cols = [5]

    if len(eval_sizes) > 0:
        sizes_plot(eval_sizes, n_contexts, similarities, experiments_name, plot_sizes, n_cols, title='')
        sizes_plot(eval_sizes, n_contexts, [ws + '_' + s for s, ws in product(similarities, sim_datasets)],
                   experiments_name, plot_sizes, n_cols, title='')

    # filter available metrics
    available_metrics = list(eval_nn.values())[0].keys()
    plot_config = [p for p in plot_config if all([m in available_metrics for m in p[1]])]

    titles, groups, metric_names, filenames, format_styles, y_lim = zip(*plot_config)
    comparison_diagram_multi(eval_nn, groups, filenames, False, format_styles, metric_names, titles, y_lim=y_lim)

    comparison_diagram_multi(eval_precision, (('cos',),),
                             titles=('Clustering',), metric_names=('cosine',),
                             filenames=('figures/{}_prec_cluster.pdf'.format(experiments_name),))

    comparison_diagram_multi(eval_recall, (('cos', ),),
                             titles=('Clustering',), metric_names=('cosine',),
                             filenames=('figures/{}_recall_cluster.pdf'.format(experiments_name),))

    names = eval_nn.keys()
    normal_names = [to_normal_name(n) for n in names]

    # now plot the legend
    fig, ax = plt.subplots()
    for n_col in n_cols:
        plt.clf()
        plt.axis('off')

        fig.set_size_inches(1 * n_col, 0.3 * (len(names) // n_col))

        plt.legend(
            [plt.Rectangle((0, 0), 10, 10, facecolor=cmap(n), hatch=hatch(n) * 2, edgecolor=[c * 0.7 for c in cmap(n)],
                           linewidth=1) for n in normal_names],
            [table_name(n, latex=False) for n in names],
            ncol=n_col)

        # plt.savefig('figures/coco_category_clusters_legend.pdf', bbox_inches='tight')
        # plt.clf()
        if filenames is None:
            plt.show()
        else:
            plt.savefig('figures/{}_legend_{}.pdf'.format(experiments_name, n_col), bbox_inches='tight')


def save_tables(eval_nn, experiments_name):

    import collections

    similarities = list(list(eval_nn.values())[0].keys())
    print(similarities)

    # evaluation table type first
    data = collections.OrderedDict()
    for i_experiment, experiment_id in enumerate(eval_nn.keys()):
        data[table_name(experiment_id, latex=False)] = {s: np.array(eval_nn[experiment_id][s]).mean() for s in similarities}

    from pandas import DataFrame
    open('figures/' + experiments_name + '_evaluation_type.csv', 'w').write(
        DataFrame(data).to_csv(sep=';', float_format='%.3f'))
    open('figures/' + experiments_name + '_evaluation_type_T.csv', 'w').write(
        DataFrame(data).T.to_csv(sep=';', float_format='%.3f'))
    open('figures/' + experiments_name + '_evaluation_type_first.tex', 'w').write(
        DataFrame(data).T.to_latex(float_format=lambda a: '%.3f' % a))
    open('figures/' + experiments_name + '_evaluation_type_first_T.tex', 'w').write(
        DataFrame(data).to_latex(float_format=lambda a: '%.3f' % a))



if __name__ == '__main__':

    # plt.bar(range(K), [1] * K, color=[cmap(n) for n in all_names])
    # plt.show()

    # cluster_group = ('cos', 'euc')
    # corr_group1 = ('coco_sim_euc', 'coco_rel_euc')
    # corr_group2 = ('coco_sim_cos', 'coco_rel_cos')
    #metric_groups = (('cos', 'euc'), ('coco_sim_cos', 'coco_sim_euc', 'coco_rel_cos', 'coco_rel_euc'))

    experiments_name = sys.argv[1]

    if experiments_name == 'precision_recall':
        pass


    eval_nn = json.load(open('figures/{}_eval_nn.json'.format(experiments_name)))
    eval_sizes = json.load(open('figures/{}_eval_sizes.json'.format(experiments_name)))
    eval_precision = json.load(open('figures/{}_eval_precision.json'.format(experiments_name)))
    eval_recall = json.load(open('figures/{}_eval_recall.json'.format(experiments_name)))

    similarities = ['cos']  # ['cos', 'euc']
    sim_datasets = ['coco_sim', 'coco_rel']
    plot_sizes = [100, 250, 1000, 5000, 10000, 50000, 250000, 700000]

    if experiments_name.endswith('mscoco'):
        n_contexts = [100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 500000, 604907]
    elif experiments_name.endswith('labelme'):
        n_contexts = [100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 325288]
    elif experiments_name.endswith('rcnn'):
        n_contexts = [100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 286835]
    else:
        n_contexts = []


    save_tables(eval_nn, experiments_name)
    save_diagrams(eval_sizes, eval_nn, eval_precision, eval_recall, experiments_name, n_contexts, similarities,
                  sim_datasets, plot_sizes)


    # comparison_diagram_multi(qualities, (cluster_group, corr_group1, corr_group2),
    #                          titles=('Clustering', 'Euclidean', 'Cosine'),
    #                          metric_names=(None, ('similarity', 'relatedness'), ('similarity', 'relatedness')),
    #                          filenames=('figures/cluster.pdf', 'figures/corr1.pdf', 'figures/corr2.pdf'),
    #                          format_styles=(' {:.1f}', '{:.3f}', '{:.3f}'))
    #

    #
    #
    # sizes_plot(qualities, n_contexts_ms, cluster_group + corr_group1, 'blubba', plot_sizes, title='Yea')
    #
    # comparison_diagram_multi(qualities, (cluster_group,), filenames=('figures/prec.pdf',))
    # comparison_diagram_multi(qualities, (cluster_group,), filenames=('figures/recall.pdf',))


