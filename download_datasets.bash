#!/bin/bash

mkdir /media/resources/MS_COCO
wget http://images.cocodataset.org/zips/train2014.zip; unzip train2014.zip; mv train2014 /media/resources/MS_COCO/train2014
wget http://images.cocodataset.org/zips/val2014.zip; unzip val2014.zip; mv val2014 /media/resources/MS_COCO/val2014
wget http://images.cocodataset.org/zips/test2015.zip; unzip test2015.zip; mv test2015 /media/resources/MS_COCO/test2015
wget http://images.cocodataset.org/annotations/annotations_trainval2014.zip; unzip annotations_trainval2014.zip; mv /media/resources/MS_COCO/annotations
wget http://nlp.stanford.edu/data/glove.6B.zip; unzip glove.6B.zip; mv glove.6B.zip /media/resources/GloVe
wget https://staff.fnwi.uva.nl/e.bruni/resources/MEN.zip; unzip MEN.zip; mv MEN /media/resources/MEN
wget https://www.cl.cam.ac.uk/~fh295/SimLex-999.zip; unzip SimLex-999.zip; mv SimLex-999 /media/resources/SimLex-999