import os
import warnings
import numpy as np
import pickle
import re

from config import TMP_PATH, GLOVE_PATH, WORD2VEC_PATH


class ConceptVectors(object):
    """ Base class that represents a set of concept vectors """

    valid_words = None

    def __init__(self):
        self.vectors = None
        self.repetitions = 1

    def name(self):
        raise NotImplementedError

    def get(self):
        raise NotImplementedError
        # return self.vectors

    def get_words(self):
        if self.vectors is None:
            self.get()

        return self.vectors.keys()

    def reduce_concepts_to(self, words):
        if self.vectors is None:
            self.get()

        self.vectors = {k: v for k, v in self.vectors.items() if k in words}

    def get_common(self, common_classes):
        common = {k: self.vectors[k] for k in list(self.vectors.keys()) if k in common_classes}
        if len(common) == 0:
            warnings.warn('Set of common vectors is empty')
        return common

    def free(self):
        """ can be implemented if necessary. """
        pass

    def recompute_on_fraction(self, fraction):
        raise ValueError('Called recompute_on_fraction method, but recomputation is not possible. Do nothing.')


class LateFusion(ConceptVectors):
    """ Late Fusion base class. """

    def __init__(self, *vecs):
        super().__init__()
        assert len(vecs) > 1, 'need more than one vector to fuse'
        self.vecs = vecs

    def name(self):
        return '{}({})'.format(self.__class__.__name__, '+'.join([v.name() for v in self.vecs]))

    def integrate(self):
        for vec in self.vecs:
            vec.integrate()

    def get_intersection(self):
        print('need to get', self.name())
        concepts = []
        for v in self.vecs:
            concepts += [set(v.get_words())]

        if len(set([len(c) for c in concepts])) == 1:
            warnings.warn('fusing different sets of concepts')

        intersecting_concepts = set(concepts[0])
        for c in concepts[1:]:
            intersecting_concepts = intersecting_concepts.intersection(c)

        return intersecting_concepts


    def get_words(self):
        words = set()
        for v in self.vecs:
            if v.vectors is None:
                v.get()
            words.update(v.get_words())

        return list(words)


class LateFusionNorm(LateFusion):

    def __init__(self, *vecs):
        super().__init__(*vecs)

    def get(self):
        if self.vectors is None:

            intersecting_concepts = self.get_intersection()

            # compute dimension-wise mean and standard deviation for every embedding
            means = [np.array(list(v.vectors.values())).mean(0) for v in self.vecs]
            stds = [np.array(list(v.vectors.values())).std(0) + 0.000001 for v in self.vecs]  # to avoid division by zero

            self.vectors = {k: np.concatenate([(v.vectors[k] - mean)/std
                                               for v, mean, std in zip(self.vecs, means, stds)]) for k in intersecting_concepts}

        return self.vectors


class LateFusionNormStdOnly(LateFusion):

    def __init__(self, *vecs):
        super().__init__(*vecs)

    def get(self):
        if self.vectors is None:

            intersecting_concepts = self.get_intersection()

            # compute dimension-wise mean and standard deviation for every embedding
            stds = [np.array(list(v.vectors.values())).std(0) + 0.000001 for v in self.vecs]  # to avoid division by zero

            self.vectors = {k: np.concatenate([v.vectors[k] / std for v, std in zip(self.vecs, stds)])
                            for k in intersecting_concepts}

        return self.vectors


class LateFusionNormMeanMag(LateFusion):

    def __init__(self, *vecs):
        super().__init__(*vecs)

    def get(self):
        if self.vectors is None:

            intersecting_concepts = self.get_intersection()

            # compute average magnitude for every embedding
            mean_mag = [np.mean(np.sqrt((np.array(list(v.vectors.values()))**2).sum(1))) for v in self.vecs]

            self.vectors = {k: np.concatenate([v.vectors[k]/m
                                               for v, m in zip(self.vecs, mean_mag)]) for k in intersecting_concepts}

        return self.vectors


class LateFusionNormToOne(LateFusion):

    def __init__(self, *vecs):
        super().__init__(*vecs)

    def get(self):
        if self.vectors is None:

            intersecting_concepts = self.get_intersection()

            self.vectors = {k: np.concatenate([v.vectors[k] / np.sqrt(v.vectors[k]**2).sum() for v in self.vecs])
                            for k in intersecting_concepts}

        return self.vectors


class LateFusionEqual(LateFusion):

    def __init__(self, *vecs):
        super().__init__(*vecs)

    def get(self):
        if self.vectors is None:

            intersecting_concepts = self.get_intersection()

            inv_lens = [1 / np.array(list(v.vectors.values())).shape[1] for v in self.vecs]
            self.vectors = {k: np.concatenate([v.vectors[k] * inv_len for v, inv_len in zip(self.vecs, inv_lens)]) for
                            k in intersecting_concepts}

        return self.vectors


class DummyConceptVectors(ConceptVectors):

    def __init__(self, words):
        super().__init__()
        for w in words:
            self.vectors[w] = np.random.uniform(0, 1, 50)


def identity(a): return a


class Word2VecVectors(ConceptVectors):

    is_initialized = False

    def __init__(self, filename, freebase_prefix=False, words=None, map_word=identity):

        super().__init__()
        self.filename = filename
        self.freebase_prefix = freebase_prefix
        self.words = words
        self.map_word = map_word

    def name(self):
        return 'W2V'

    def initialize(self):

        if self.map_word.__name__ == '<lambda>':
            raise ValueError('Anonymous functions are not allowed.')

        try:
            w2v_dictionary = pickle.load(open(os.path.join(TMP_PATH, 'w2v_all_words.pickle'), 'rb'))
            w2v_dictionary = {k: v for k, v in list(w2v_dictionary.items())}
        except FileNotFoundError:
            from gensim.models import KeyedVectors

            # categories = json.load(open(this_path+'data/categories.txt'))

            print('load w2v model...')
            model = KeyedVectors.load_word2vec_format(os.path.join(WORD2VEC_PATH, self.filename), binary=True)
            print('done')

            w2v_dictionary = {}
            freebase_skip = 4 if self.freebase_prefix else 0

            if self.words is None:
                self.words = list(model.vocab.keys())

            print('parsing', len(self.words), 'words')

            for w in self.words:

                k = w[freebase_skip:]
                k = self.map_word(k)

                try:
                    if k not in w2v_dictionary and k is not None:
                        w2v_dictionary[k] = np.array(model[w].tolist(), dtype='float32')
                    else:
                        warnings.warn('The word ' + (k if k is not None else '') + ' is already in the dictionary.')
                except KeyError:
                    warnings.warn('The word ' + w + ' is not contained in word2vec')

            print(('word2vec: extracted ', len(w2v_dictionary), 'word vectors'))

            pickle.dump(w2v_dictionary, open(os.path.join(TMP_PATH, 'w2v_all_words.pickle'), 'wb'))

        # return an array
        self.is_initialized = True
        self.vectors = dict({k: np.array(v) for k, v in list(w2v_dictionary.items())})

    def get(self):
        if self.is_initialized:
            return self.vectors
        else:
            self.initialize()
            return self.vectors


class GloveVectors(ConceptVectors):

    is_initialized = False
    cache = False

    def __init__(self, filename='glove.6B.50d.txt', words=None, map_word=lambda a: a):
        super().__init__()
        self.filename = filename
        self.words = words
        self.map_word = map_word

    def name(self):
        return 'Glove'

    def initialize(self):

        try:
            with open(os.path.join(TMP_PATH, 'glove_all_words.pickle'), 'rb') as f:
                glove_dictionary = pickle.load(f)
        except FileNotFoundError:
            #categories = json.load(open(this_path+'data/categories.txt'))
            glove = open(os.path.join(GLOVE_PATH, self.filename)).read()
            #glove = open(this_path+'../../software/glove/vectors.txt').read()

            # glove = glove.decode('utf8')

            glove = glove.split('\n')
            splits = [g.split(' ') for g in glove]

            glove_dictionary = {}
            for s in splits[:-1]:
                k = s[0]
                if self.words is None or k in self.words:
                    k = self.map_word(k)
                    if k is not None:
                        if k not in glove_dictionary:
                            glove_dictionary[k] = np.array([float(v) for v in s[1:]], dtype='float32')
                        else:
                            warnings.warn('The word ' + k + ' is already in the dictionary.')

            print(('GloVe: extracted ', len(glove_dictionary), 'word vectors'))

            with open(os.path.join(TMP_PATH, 'glove_all_words.pickle'), 'wb') as f:
                pickle.dump(glove_dictionary, f)

        self.vectors = dict({k: np.array(v) for k, v in list(glove_dictionary.items())})
        self.is_initialized = True

    def get(self):
        if not self.is_initialized:
            self.initialize()

        return self.vectors


class Random(ConceptVectors):
    is_initialized = False
    cache = False

    def __init__(self, scenes, dim=100):
        super().__init__()
        self.dim = dim
        self.repetitions = 25
        self.scenes = scenes

    def name(self):
        return 'Random'

    def integrate(self, n_contexts=None):
        self.initialize()

    def initialize(self):
        self.vectors = dict({k: np.random.uniform(0, 1, self.dim) for k in self.scenes.all_occurring_objects()})
        self.is_initialized = True

    def get(self):
        if not self.is_initialized:
            self.initialize()

        return self.vectors


class KielaVectors(ConceptVectors):

    is_initialized = False

    def __init__(self, folder, kiela_type, words=None, map_word=identity):
        super().__init__()
        self.folder = folder
        self.kiela_type = kiela_type
        self.words = words
        self.map_word = map_word
        self.vectors = None

    def initialize(self):

        assert(self.kiela_type in ['max', 'mean'])

        try:
            print('loading kiela vectors from disk')
            with open(os.path.join(TMP_PATH, 'kiela_{}_all_words.pickle'.format(self.kiela_type)), 'rb') as f:
                self.vectors = pickle.load(f)

        except FileNotFoundError:
            print('loading kiela vectors')
            files = os.listdir(self.folder)
            print(len(files), 'files')

            self.vectors = {}
            for i, filename in enumerate(files):
                match = re.match(r'^([\w]*)-' + self.kiela_type + '\.npy$', filename)

                if match and (self.words is None or match.group(1) in self.words):
                    vector = np.load(os.path.join(self.folder, match.group(0)))
                    # print match.group(1), vector.dtype, vector.shape
                    self.vectors[self.map_word(match.group(1))] = vector

                if i % 1000 == 999:
                    print(i / float(len(files))*100, '%')

            assert len(self.vectors) > 0

        self.is_initialized = True

    def get(self):
        if self.vectors is None:
            self.initialize()

        assert len(self.vectors) > 0
        return self.vectors


class KielaMaxVectors(KielaVectors):

    def __init__(self, folder,  words=None, map_word=identity):
        super(KielaMaxVectors, self).__init__(folder, 'max', words, map_word)

    def name(self):
        return 'KielaMax'


class KielaMeanVectors(KielaVectors):

    def __init__(self, folder, words=None, map_word=identity):
        super(KielaMeanVectors, self).__init__(folder, 'mean', words, map_word)

    def name(self):
        return 'KielaMean'
