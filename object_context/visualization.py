import os

from logging import warning
from matplotlib.colors import ListedColormap
from matplotlib.font_manager import FontProperties
from scipy.spatial.distance import pdist, squareform
from diagrams import to_normal_name, cmap
from sklearn.manifold import MDS
import numpy as np
import matplotlib.pyplot as plt


color_list = ListedColormap(['#629169', '#8d384d', '#9f6da9', '#3d526c', '#b5aa5a' , '#6e99b3', '#a8553a', '#63ab8c', '#aaaaaa', '#e1e1e1', '#333333'])


def cluster_visualization(vector_space, category_map, distance='cos', filename=None):

    vectors = vector_space.get()

    plt.clf()
    fig, ax = plt.subplots()
    fig.set_size_inches(5, 5)
    plt.axis('off')

    if len(set(vectors.keys()).difference(list(category_map.keys()))) > 0:
        ValueError('For some words no category is specified.')

    selected_vectors, categories = list(zip(*[(v, category_map[k]) for k, v in list(vectors.items()) if k in category_map]))
    selected_vectors = np.array(selected_vectors)
    # plt.title('category '+name)

    print('mds shape', selected_vectors.shape)

    if distance == 'cos':
        pairwise_distances = squareform(pdist(selected_vectors, 'cosine'))
    elif distance == 'euc':
        pairwise_distances = squareform(pdist(selected_vectors, 'euclidean'))
    elif distance == 'dot':
        pairwise_distances = np.outer(selected_vectors, selected_vectors)
    else:
        raise ValueError('Invalid distance type: ' + distance)

    vectors2d = MDS(dissimilarity='precomputed').fit_transform(pairwise_distances)

    plt.scatter(
        vectors2d[:, 0]/vectors2d.max(),
        vectors2d[:, 1]/vectors2d.max(),
        c=categories,
        linewidth=0,
        edgecolor='none',
        marker='o', s=75,
        cmap=color_list
    )

    if filename is None:
        plt.show()
    else:
        plt.savefig(os.path.join('figures', filename), bbox_inches='tight')
    plt.clf()


def text_visualization(vector_space, selection, distance='cos', filename=None):

    vectors = vector_space.get()

    plt.clf()
    fig, ax = plt.subplots()
    fig.set_size_inches(5, 5)
    plt.axis('off')


    if any([k not in vectors for k in selection]):
        warning('can not plot because of missing concepts')
        return

    selected_vectors = np.array([vectors[k] for k in selection])

    print('mds shape text vectors', selected_vectors.shape)
    # object_vectors = object_vectors/(10*object_vectors.std())

    if distance == 'cos':
        pairwise_distances = squareform(pdist(selected_vectors), 'cosine')
    elif distance == 'euc':
        pairwise_distances = squareform(pdist(selected_vectors), 'euclidean')
    elif distance == 'dot':
        v = selected_vectors
        pairwise_distances = np.outer(v, v)
    else:
        raise ValueError('Invalid distance type: ' + distance)

    # print('pairwise dist', pairwise_distances.shape)
    # print(pairwise_distances.flatten())

    vectors_new = MDS(dissimilarity='precomputed').fit_transform(pairwise_distances)

    for (x, y), o in zip(vectors_new, selection):
        plt.text(x, y, o, fontsize=15)

    plt.scatter([a[0] for a in vectors_new], [a[1] for a in vectors_new], color=(1.0, 0, 0, 0.0))


    if filename is None:
        plt.show()
    else:
        plt.savefig(os.path.join('figures', filename), bbox_inches='tight')
    plt.clf()


def trait_visualization(conf, traits, classes):
    vecs = conf
    vecs.get()
    vecs.reduce_concepts_to(classes)

    font_title = FontProperties()
    font_title.set_family('Lato')
    font_title.set_weight('medium')

    concepts, vectors = zip(*vecs.vectors.items())

    vectors = np.array(vectors)
    pairwise_distances = squareform(pdist(vectors, 'cosine'))
    vectors2d = MDS(dissimilarity='precomputed').fit_transform(pairwise_distances)

    color = cmap(to_normal_name(conf.name()))

    for trait, selected_concepts in traits.items():
        indices = [concepts.index(l) for l in selected_concepts if l in concepts]
        selected_concepts = [l for l in selected_concepts if l in concepts]

        fig, ax = plt.subplots()
        fig.set_size_inches(5, 5)

        plt.axis('off')
        plt.title(trait, fontproperties=font_title, fontsize=30)

        other_indices = [i for i in range(len(concepts)) if i not in indices]
        plt.scatter(*vectors2d[other_indices].T, c='gray', s=50)

        plt.scatter(*vectors2d[indices].T, c=[color], s=80)
        assert len(vectors2d[indices]) == len(selected_concepts)
        for (x, y), t in zip(vectors2d[indices], selected_concepts):
            plt.text(x + 0.005, y + 0.005, t, fontsize=22, color='white')
            plt.text(x + 0.002, y + 0.002, t, fontsize=22, color=color)

        plt.savefig('figures/trait_{}_{}.pdf'.format(conf.name(), trait))
        # plt.show()
        # plt.waitforbuttonpress()
        plt.clf()
