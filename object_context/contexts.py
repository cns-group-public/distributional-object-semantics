import itertools
from collections import Counter
from queue import Queue
from threading import Lock, Thread

import numpy as np
import skimage.draw as draw
from nltk.corpus import wordnet
from shapely.geometry import Polygon, MultiPolygon
from shapely.topology import TopologicalError
from skimage.io import imread
from skimage.transform import resize

from network_configuration import NETWORK_CONFIGURATIONS, normalize

try:
    import cv2
    OPENCV_AVAILABLE = True
    opencv = cv2
except ImportError:
    OPENCV_AVAILABLE = False


class ContextBase(object):
    """ Base class for all context models. """

    cache = False
    nan_counter = 0
    compute_shapes = False

    def extract(self, object_classes_img, shapes, diagonal_length, obj2idx, el2idx):
        pass

    def expand_category(self, category):
        return []

    def preprocess_pre(self):
        self.obj2idx = {}

    def preprocess_scene(self, item):
        pass

    def preprocess_complete(self):
        pass

    def process_scene(self, item):
        pass

    def postprocessing(self, vectors):
        return vectors


class DummyContext(ContextBase):
    name = 'Dummy'
    obj2idx = {}
    next_category_idx = 0
    cache = False

    def __init__(self, k=100, contexts_per_scene=15):
        assert type(contexts_per_scene) == int

        self.contexts_per_scene = contexts_per_scene
        self.name = 'Dummy' + str(k)  # str(np.random.randint(0, 999))
        self.k = k

    def process_scenes(self, scenes, obj2idx, el2idx):
        objects, contexts = [], []
        for _ in scenes:
            objects += [np.random.randint(0, len(obj2idx)) for _ in range(self.contexts_per_scene)]
            contexts += [np.random.uniform(0, 1, self.k) for _ in range(self.contexts_per_scene)]

        return objects, contexts, False


class DCNNContextBaseTF(ContextBase):
    """ Base class for visual-feature based context. """

    name = 'DCNN'
    cache = True
    dtype = 'float32'
    obj2idx = {}
    initialized = False
    network_config_key = None
    BATCH_SIZE = 64

    def __init__(self, bounding_box_mode=True):
        cfg = NETWORK_CONFIGURATIONS[self.network_config_key]
        self.net, self.scope, self.net_args, self.checkpoint, self.layer, self.mean, self.std, self.range, self.img_size, _ = cfg
        self.bounding_box_mode = bounding_box_mode
        self.tmp_count = 0

        import tensorflow as tf
        from tensorflow.contrib import slim
        from tensorflow.contrib.framework import assign_from_checkpoint_fn, get_variables_to_restore

        tf.reset_default_graph()

        with slim.arg_scope(self.scope()):
            self.inp = tf.placeholder(tf.float32, (self.BATCH_SIZE, self.img_size, self.img_size, 3), name='input')
            self.out, activations = self.net(self.inp, is_training=False, **self.net_args)

            init_fn = assign_from_checkpoint_fn('models/{}'.format(self.checkpoint), get_variables_to_restore(),
                                                ignore_missing_vars=False)

            self.activation = activations[self.layer]

        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        init_fn(self.sess)
        self.lock = Lock()
        self.active = True
        self.occurring_labels = []
        self.network_thread = None
        self.parallel = False

    def network_worker(self, queue, objects, contexts, obj2idx):

        while self.active:
            if queue.qsize() > self.BATCH_SIZE:
                self.run_network(queue, objects, contexts, obj2idx)

    def run_network(self, obj_img_queue, objects, contexts, obj2idx, force_run=False):
        qsize = obj_img_queue.qsize()
        n_iterations = qsize // self.BATCH_SIZE

        if force_run:
            n_iterations = max(1, n_iterations)

        for _ in range(n_iterations):
            images, labels = [], []

            valid_samples = min(qsize, self.BATCH_SIZE)

            for _ in range(valid_samples):
                label, img = obj_img_queue.get()
                images += [img]
                labels += [label]

            if force_run:
                valid_samples = qsize
                for _ in range(self.BATCH_SIZE - valid_samples):
                    images += [np.zeros((self.img_size, self.img_size, 3), 'float32')]
                    labels += [None]
            else:
                valid_samples = self.BATCH_SIZE

            images = np.array(images)
            features = self.sess.run(self.activation, {self.inp: images})
            features = features.reshape((self.BATCH_SIZE, -1))
            features = features[:valid_samples]  # because batch size is 1

            if not any(np.isnan(features.flatten())):
                with self.lock:
                    objects.extend([obj2idx[l] for l in labels[:valid_samples]])
                    contexts.extend(features.astype(self.dtype))
            else:
                self.nan_counter += 1

    def process_scenes(self, scenes, obj2idx, el2idx):
        obj_img_queue = Queue()
        objects, contexts = [], []

        if self.parallel:
            network_thread = Thread(target=self.network_worker, args=(obj_img_queue, objects, contexts, obj2idx))
            network_thread.start()

        try:
            for i_scene, (filename, items) in enumerate(scenes):

                if i_scene % 100 == 99:
                    if len(contexts) > 5:
                        print(np.array(contexts[-5:]).min(), np.array(contexts[-5:]).max(), np.array(contexts[-5:]).dtype)
                    print('image {} has {} objects ({}/{})'.format(filename, len(items), i_scene, len(scenes)))

                img_orig = imread(filename)
                scale_y, scale_x = self.img_size / img_orig.shape[0], self.img_size / img_orig.shape[1]

                if OPENCV_AVAILABLE:
                    img_orig = opencv.resize(img_orig / 255, (self.img_size, self.img_size))
                else:
                    img_orig = resize(img_orig / 255, (self.img_size, self.img_size))

                if img_orig.ndim == 2:
                    img_orig = np.dstack([img_orig] * 3)

                if img_orig.shape[2] == 4:
                    img_orig = img_orig[:, :, :3]

                for label, polygons in items:

                    img = img_orig.astype('float32', copy=True)

                    assert len(polygons) >= 1

                    # assume polygon is just a bbox
                    # [x1, y1, x2, y2, x3, y3, ...]
                    for polygon in polygons:
                        X = (np.array(polygon[0::2]) * scale_x).astype('int')
                        Y = (np.array(polygon[1::2]) * scale_y).astype('int')
                        img[int(min(Y)):int(max(Y)), int(min(X)):int(max(X))] = 0

                    # normalize
                    img = normalize(img, self.mean, self.std, self.range)

                    assert img.shape == (self.img_size, self.img_size, 3)

                    obj_img_queue.put((label, img))

                    if not self.parallel:
                        self.run_network(obj_img_queue, objects, contexts, obj2idx)

            if self.parallel:
                self.active = False
                network_thread.join()

            print('last batch')
            self.run_network(obj_img_queue, objects, contexts, obj2idx, force_run=True)
            print('extracted {} contexts from {} scenes'.format(len(contexts), len(scenes)))

            is_skipped = False
        except KeyboardInterrupt:
            self.active = False
            network_thread.join()
            is_skipped = True

        return objects, contexts, is_skipped


class ResNet50Context(DCNNContextBaseTF):
    name = 'ResNet50'
    network_config_key = 'resnet50'


class VGG16Context(DCNNContextBaseTF):
    name = 'VGG16'
    network_config_key = 'vgg16'


class InceptionV4Context(DCNNContextBaseTF):
    name = 'InceptionV4'
    network_config_key = 'inception_v4'


class InceptionV1Context(DCNNContextBaseTF):
    name = 'InceptionV1'
    network_config_key = 'inception_v1'



class CoContextBase(ContextBase):
    """ Base class for label-based context. """

    obj2idx = {}
    next_category_idx = 0
    expansion_categories = []

    # per scene
    shapes = []
    object_classes_img = []
    count = False
    self_count = False

    def __init__(self):
        self.class_counter = Counter()

    # @profile
    def process_scenes(self, scenes, obj2idx, el2idx):

        objects, contexts = [], []
        for (_, items) in scenes:
            shapes = []
            object_classes_img = []

            for item in items:
                item_category, segments = item
                for category in [item_category] + self.expand_category(item_category):

                    polygons = []
                    if self.compute_shapes:
                        # calculate the object size first
                        for segment in segments:
                            x = segment[::2]
                            y = segment[1::2]
                            if len(x) > 3 and len(y) > 3:
                                polygons += [Polygon(list(zip(x, y)))]

                        polygons = MultiPolygon(polygons)
                        shapes += [polygons]

                    if not self.compute_shapes or len(polygons) > 0:
                        object_classes_img += [category]

            diagonal_length = 0.0
            if self.compute_shapes:
                #TODO: what does the 50.0 mean? Can this parameter be replaced?
                diagonal_length = max(50.0, max([s.distance(s2) for s2 in self.shapes for s in self.shapes]))

            scene_o, scene_c = self.extract(object_classes_img, shapes, diagonal_length, obj2idx, el2idx)
            objects += scene_o
            contexts += scene_c

        # last item is was_skipped
        return objects, contexts, False

    def extract(self, object_classes_img, shapes, diagonal_length, obj2idx, el2idx):

        origin_object_classes = np.array([obj2idx[c] for c in object_classes_img if c in obj2idx])
        n_original_objects = len(origin_object_classes)
        context_object_classes = np.array([el2idx[c] for c in object_classes_img])

        if len(origin_object_classes) > 0 and len(context_object_classes) > 0:
            new_contexts = np.zeros((n_original_objects, len(el2idx)), self.dtype)
            comb = list(itertools.product(range(len(origin_object_classes)), context_object_classes))

            if not self.self_count:
                comb = [(i, b) for (i, b) in comb if origin_object_classes[i] != b]

            if self.count:
                comb_count = [(a, b, c) for (a, b), c in Counter(comb).items()]
            else:
                comb_count = [(a, b, 1) for a, b in comb]

            if len(comb_count) > 0:
                obj, con, count = zip(*comb_count)
                new_contexts[obj, con] = count

            # slow version
            # new_contexts2 = np.zeros((n_original_objects, len(el2idx)), self.dtype)
            # for i, c_i in enumerate(original_object_classes):
            #     for c_j in expanded_object_classes:
            #         if c_i != c_j or self.self_count:
            #
            #             if self.count:
            #                 new_contexts2[i, c_j] += 1
            #             else:
            #                 new_contexts2[i, c_j] = 1
            # assert (new_contexts2 == new_contexts).all()

        context_list = [new_contexts[i] for i in range(n_original_objects)]
        return origin_object_classes.tolist(), context_list


class CoCountContextSelf(CoContextBase):
    name = 'CCs'
    self_count = True
    count = True
    dtype = 'uint8'
    compute_shapes = False


class CoCountContextNoself(CoContextBase):
    name = 'CCn'
    self_count = False
    count = True
    dtype = 'uint8'
    compute_shapes = False


class CoOccurrenceContextSelf(CoContextBase):
    name = 'COs'
    self_count = True
    count = False
    dtype = 'uint8'
    compute_shapes = False


class CoOccurrenceContextNoself(CoContextBase):
    name = 'COn'
    self_count = False
    count = False
    dtype = 'uint8'
    compute_shapes = False


class HypernymExpansion(object):

    def expand_category(self, category):
        try:
            expansion = [a.name() for a in wordnet.synset(category).hypernym_paths()[0][-3:]]
            expansion.remove(category)
            return expansion
        except ValueError:
            return []


class CoOccurrenceContextNoselfHypernymExpansion(HypernymExpansion, CoOccurrenceContextNoself):
    name = 'COn_H+'


class CoOccurrenceContextSelfHypernymExpansion(HypernymExpansion, CoOccurrenceContextSelf):
    name = 'COs_H+'


class CoCountContextNoselfHypernymExpansion(HypernymExpansion, CoCountContextNoself):
    name = 'CCn_H+'


class CoCountContextSelfHypernymExpansion(HypernymExpansion, CoCountContextSelf):
    name = 'CCs_H+'
