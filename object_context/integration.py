from collections import OrderedDict
from random import shuffle
import numpy as np
import pickle
import os
import tensorflow as tf

from config import TMP_PATH
from .concept_vectors import ConceptVectors


class IntegrationBase(ConceptVectors):
    """ Base class for the integration from a set of contexts into concept vectors """

    def __init__(self, extractor):
        super().__init__()
        self.extractor = extractor
        self.change_zero_vectors = True

    def name(self):
        return '{}_{}'.format(self.__class__.__name__, self.extractor.name())

    def integrate(self, n_contexts=None):
        raise NotImplementedError

    def get_cache_filename(self, size=None):
        if size is not None:
            filename = os.path.join(TMP_PATH, self.name() + '@' + str(size) + '.pickle')
        else:
            filename = os.path.join(TMP_PATH, self.name() + '.pickle')
        return filename

    def load_from_disk(self, size=None):
        filename = self.get_cache_filename(size)

        if not os.path.isdir(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))

        self.vectors = pickle.load(open(filename, 'rb'))
        print('loaded vectors from disk: {}'.format(filename))

    def save_to_disk(self, size):
        if self.extractor.cache:
            filename = self.get_cache_filename(size)
            pickle.dump(self.vectors, open(filename, 'wb'))
            print('saved vectors to disk: {}'.format(filename))
        else:
            print('do not save vectors')

    def get_words(self):
        return self.extractor.scene_model.all_occurring_objects()

    def free(self):
        self.extractor.objects = None
        self.extractor.contexts = None
    # def __add__(self, other):
    #     return super().__add__(other)

    def get(self):
        if self.vectors is None:
            self.integrate()
        return self.vectors

    def apply(self, mapping):
        self.mapping = mapping
        super().apply(self.mapping)
        return self

    def set_change_zero_vectors(self):
        self.change_zero_vectors = True
        return self

    def zero_vectors_change(self):
        if self.change_zero_vectors:
            self.vectors = {k: 0.0001 * np.ones(v.shape) if np.all(np.array(v) == np.zeros(v.shape)) else v
                            for k, v in list(self.vectors.items()) if k}



class Averaging(IntegrationBase):
    """ Integrate contexts by averaging them. """

    def __init__(self, extractor):
        super().__init__(extractor)

    def integrate(self, n_contexts=None, size_per_concept=None):

        try:
            if self.extractor.cache:
                self.load_from_disk(n_contexts)
            else:
                raise FileNotFoundError('Context is not using cache')
        except FileNotFoundError:
            print('precomputed vectors of {} for n_contexts {} were not found.'.format(
                self.extractor.name(), n_contexts))

            if self.extractor.contexts is None:  # if contexts are not extracted yet
                print('need to extract contexts')
                self.extractor.extract()

            self.vectors = {o: None for o in list(self.extractor.obj2idx.keys())}

            if n_contexts is not None:
                selected = np.zeros(len(self.extractor.objects), dtype='bool')

                err_msg = 'Number of extracted contexts {} is smaller than required size {}'.format(
                    len(self.extractor.contexts), n_contexts)
                assert len(self.extractor.contexts) >= n_contexts, err_msg
                selected[np.random.choice(len(self.extractor.objects), n_contexts, replace=False)] = True
            else:
                selected = True

            for concept_name, object_idx in list(self.extractor.obj2idx.items()):
                this_contexts = []
                for i in np.where((self.extractor.objects == object_idx) & selected)[0]:
                    this_contexts += [self.extractor.contexts[i]]

                if size_per_concept is not None:
                    shuffle(this_contexts)
                    this_contexts = this_contexts[size_per_concept]

                if len(this_contexts) > 0:
                    self.vectors[concept_name] = np.mean(np.array(this_contexts), 0)
                else:
                    # assign a random vector if the concept does not exist
                    self.vectors[concept_name] = np.random.uniform(-1, 1, self.extractor.context_size)

            self.zero_vectors_change()
            self.save_to_disk(n_contexts)


class Median(IntegrationBase):
    """ Integrate contexts by computing the dimension-wise median. """

    def __init__(self, extractor):
        super().__init__(extractor)

    def integrate(self, n_contexts=None, size_per_concept=None):

        try:
            if self.extractor.cache:
                self.load_from_disk(n_contexts)
            else:
                raise FileNotFoundError('Context is not using cache')
        except FileNotFoundError:
            print('precomputed vectors of {} for n_contexts {} were not found.'.format(
                self.extractor.name(), n_contexts))

            if self.extractor.contexts is None:  # if contexts are not extracted yet
                print('need to extract contexts')
                self.extractor.extract()

            self.vectors = {o: None for o in list(self.extractor.obj2idx.keys())}

            if n_contexts is not None:
                selected = np.zeros(len(self.extractor.objects), dtype='bool')

                err_msg = 'Number of extracted contexts {} is smaller than required size {}'.format(
                    len(self.extractor.contexts), n_contexts)
                assert len(self.extractor.contexts) >= n_contexts, err_msg
                selected[np.random.choice(len(self.extractor.objects), n_contexts, replace=False)] = True
            else:
                selected = True

            for concept_name, object_idx in list(self.extractor.obj2idx.items()):
                this_contexts = []
                for i in np.where((self.extractor.objects == object_idx) & selected)[0]:
                    this_contexts += [self.extractor.contexts[i]]

                if size_per_concept is not None:
                    shuffle(this_contexts)
                    this_contexts = this_contexts[size_per_concept]

                if len(this_contexts) > 0:
                    self.vectors[concept_name] = np.median(np.array(this_contexts), 0)
                else:
                    self.vectors[concept_name] = np.random.uniform(-1, 1, self.extractor.context_size)

            self.zero_vectors_change()
            self.save_to_disk(n_contexts)


class SkipGram(IntegrationBase):
    """
    Implementation of skip gram negative sampling for context integration.
    This roughly follows the tensorflow tutorial:
    https://www.tensorflow.org/tutorials/word2vec
    """

    def __init__(self, extractor, n_negatives=25, epochs=50, embedding_size=64):
        super().__init__(extractor)
        self.embedding_size = embedding_size
        self.epochs = epochs
        self.repetitions = 10
        self.n_negatives = n_negatives

    def name(self):
        return 'SkipGram_neg{}_ep{}_dim{}_{}'.format(self.n_negatives, self.epochs, self.embedding_size,
                                                     self.extractor.name())

    def integrate(self, n_contexts=None):
        try:
            self.load_from_disk(n_contexts)
            print('loaded vectors from cache')
        except FileNotFoundError:
            if self.extractor.contexts is None:  # if contexts are not extracted yet
                print('need to extract contexts')
                self.extractor.extract()

            print('Skip Gram, number of contexts: {}, avg. context size: {}'.format(
                len(self.extractor.contexts), np.mean([len(c) for c in self.extractor.contexts])))

            obj2idx = self.extractor.obj2idx
            classes = [w for w, _ in sorted(obj2idx.items(), key=lambda x:x[1])]
            # classes = list(self.extractor.obj2idx.keys())
            self.vectors = OrderedDict([(o, None) for o in list(self.extractor.obj2idx.keys())])

            n_classes = len(obj2idx)
            N_REPEAT = 2

            samples = []
            for obj, con in zip(self.extractor.objects, self.extractor.contexts):
                # print(obj, np.argwhere(con > 0)[:, 0])

                if type(con) == list and len(con) == 1 and type(con[0]) == np.ndarray and con[0].dtype == 'uint8':
                    context_elements = np.argwhere(con[0] > 0)[:, 0]
                elif type(con) == np.ndarray and con.dtype == 'uint8':
                    context_elements = np.argwhere(con > 0)[:, 0]
                elif type(con) == list:
                    context_elements = con
                else:
                    raise ValueError('unexpected type of contexts: ' + str(con.dtype))

                if len(context_elements) >= N_REPEAT:
                    samples += [(obj, context_elements)]

            shuffle(samples)
            samples = samples[:n_contexts]

            # compute skip-gram vectors
            EMBEDDING_SIZE = self.embedding_size
            BATCH_SIZE = 128
            NUM_SAMPLED = self.n_negatives  # negative examples to sample
            N_EPOCHS = self.epochs

            assert BATCH_SIZE % N_REPEAT == 0

            # graph definition
            embeddings = tf.Variable(tf.random_uniform([n_classes, EMBEDDING_SIZE], -1.0, 1.0))
            nce_weights = tf.Variable(tf.truncated_normal([n_classes, EMBEDDING_SIZE], stddev=1.0 / np.sqrt(EMBEDDING_SIZE)))
            nce_biases = tf.Variable(tf.zeros([n_classes]))

            # inputs
            inp_contexts = tf.placeholder(tf.int32, shape=[BATCH_SIZE])
            inp_labels = tf.placeholder(tf.int32, shape=[BATCH_SIZE, 1])

            # to obtain nearest neighbors
            if len(classes) > 250:
                val_samples = [1, 31, 38, 45, 57, 62, 100, 125, 200, 250]
            else:
                val_samples = [1, 2, 5, 9, 12, 20, 35, 42, 51, 72]

            embed = tf.nn.embedding_lookup(embeddings, inp_contexts)
            norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True))
            normalized_embeddings = embeddings / norm
            valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings, tf.constant(val_samples, dtype=tf.int32))
            similarity = tf.matmul(valid_embeddings, normalized_embeddings, transpose_b=True)

            loss = tf.reduce_mean(tf.nn.nce_loss(weights=nce_weights, biases=nce_biases, labels=inp_labels,
                                                 inputs=embed, num_sampled=NUM_SAMPLED, num_classes=n_classes))

            optimizer = tf.train.GradientDescentOptimizer(1).minimize(loss)
            # optimizer = tf.train.RMSPropOptimizer(0.1).minimize(loss)

            init = tf.global_variables_initializer()

            indices = list(range(len(samples)))
            shuffle(indices)

            def maybe_random_sample(a, n):
                if len(a) > n:
                    return np.random.choice(a, n, replace=False)
                else:
                    return a

            session = tf.Session()
            with session:
                init.run()

                step = 0
                for i_epoch in range(N_EPOCHS):

                    losses = []
                    n_batches = len(indices) // (BATCH_SIZE * N_REPEAT)
                    print('epoch', i_epoch, 'n_batches', n_batches)
                    for i_batch in range(n_batches):

                        sample_size = BATCH_SIZE // N_REPEAT
                        batch_indices = list(range(i_batch * sample_size, (i_batch + 1) * sample_size))

                        batch_objects = np.array([samples[i][0] for i in batch_indices for _ in range(N_REPEAT)])
                        batch_objects = batch_objects.reshape((BATCH_SIZE, 1))

                        # randomly sample N_REPEAT contexts from all contexts in the samples
                        batch_contexts = np.array([c for i in batch_indices for c in maybe_random_sample(samples[i][1], N_REPEAT)])

                        run_metadata = tf.RunMetadata()
                        _, loss_val = session.run([optimizer, loss],
                                                  {inp_contexts: batch_contexts, inp_labels: batch_objects},
                                                  run_metadata=run_metadata)
                        step += 1
                        losses += [loss_val]
                        if n_batches // 10 > 0 and i_batch % (n_batches // 10) == (n_batches // 10) - 1:
                            print('step {}, loss: {}'.format(step, np.mean(losses)))
                            losses = []

                        if n_batches // 3 > 0 and i_batch % (n_batches // 3) == (n_batches // 3) - 1:
                            similarities = similarity.eval()

                            for i_selected in range(len(val_samples)):
                                q = val_samples[i_selected]
                                print('closest of "{}": {}'.format(
                                    classes[q], ', '.join([classes[j] for j in np.argsort(-similarities[i_selected])[0:6]]))
                                )

                    shuffle(indices)

                vectors, = session.run([embeddings])
                print(vectors.shape)

            print('completed skip-gram computation')

            for cls, vec in zip(classes, vectors):
                self.vectors[cls] = vec

            self.save_to_disk(n_contexts)
