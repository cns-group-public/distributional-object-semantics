import numpy as np
from collections import Counter
from scipy.stats import spearmanr, mode
from scipy.spatial.distance import euclidean
import os
from warnings import warn

import itertools

from config import MEN_PATH, SIMLEX_PATH

this_path = os.path.dirname(__file__)+'/'


def evaluate_correlation_csv_dataset(lines, vector_dictionary, similarity,
                                     start_line=1, positions=(0, 1, 2), proper_line_lenght=3, proper_n_pairs=None,
                                     sep=',', mapping=None):

    # TODO: check for correctness
    if similarity == 'euc':
        similarity_function = lambda a, b: -1*np.sqrt(((a-b)**2).sum())
    elif similarity == 'dot':
        similarity_function = lambda a, b: a.dot(b)
    elif similarity == 'cos':
        similarity_function = lambda a, b: a.dot(b)/(np.sqrt((a**2).sum()) * np.sqrt((b**2).sum()) + 0.00001)

    wordsim_pairs = {}
    wordsim_words = set()
    for l in lines[start_line:]:
        l = l.replace('\r', '')
        l = l.replace('\n', '')
        #l = l.replace(' ', '')
        items = l.split(sep)
        items = [it.strip() for it in items]
        if len(items) == proper_line_lenght:
            a = items[positions[0]]
            b = items[positions[1]]
            s = items[positions[2]]

            if mapping is not None:
                if type(mapping) == dict and a in mapping and b in mapping:
                    a, b = (mapping[a], mapping[b])
                elif hasattr(mapping, '__call__'):
                    a, b = (mapping(a), mapping(b))
                else:
                    a, b = (None, None)

            if a is not None and b is not None:
                wordsim_words.add(a)
                wordsim_words.add(b)
                wordsim_pairs[(a, b)] = float(s)

    # print sep, wordsim_words
    common_words = wordsim_words.intersection(list(vector_dictionary.keys()))
    print('similarity:', similarity, 'common words:', len(common_words))

    # print(common_words)
    # print 'missing from vector dictionary', list(set(wordsim_words).difference(vector_dictionary.keys()))[:250]
    # print 'missing from wordsim', list(set(vector_dictionary.keys()).difference(wordsim_words))[:250]

    wordsim_similarities = []
    vector_similarities = []

    pair_word_counter = Counter()
    pairs = []

    for i, (w1, w2) in enumerate(itertools.product(common_words, common_words)):
        if (w1, w2) in wordsim_pairs:
            pair_word_counter[w1] += 1
            pair_word_counter[w2] += 1
            pairs += [(w1, w2)]
            wordsim_similarities += [wordsim_pairs[(w1, w2)]]
            vector_similarities += [similarity_function(vector_dictionary[w1], vector_dictionary[w2])]

    if len(wordsim_similarities) > 2:

        print(len(wordsim_similarities), 'common word pairs')

        if len(wordsim_similarities) < 25:
            warn('Small number of common word pairs (' + str(len(wordsim_similarities)) +'). Correlation might not be reliable.')

        # return pearsonr(wordsim_similarities, vector_similarities)

        corr, p_value = spearmanr(wordsim_similarities, vector_similarities)
        print('p value:', p_value)
        return corr
    else:
        print('words from dataset', wordsim_words)
        print('words from vector', list(vector_dictionary.keys()))
        raise ValueError('Insufficient common pairs: ' + str(len(wordsim_similarities)))


def scene250map(w):
    m = {'remote control': 'remote', 'computer mouse': 'mouse'}
    return m[w] if w in m else w


def evaluate_correlation(sim_type, vector_dictionary, similarity):
    print(sim_type)

    if sim_type == 'MEN':
        f = open(os.path.join(MEN_PATH, 'MEN_dataset_natural_form_full'))
        lines = f.read().split('\n')
        return evaluate_correlation_csv_dataset(lines, vector_dictionary, similarity, sep=' ')
    elif sim_type == 'coco_sim':
        f = open('data/scene250_averaged.txt')
        lines = f.read().split('\n')
        return evaluate_correlation_csv_dataset(lines, vector_dictionary, similarity, start_line=0, proper_line_lenght=4, proper_n_pairs=250,
                                                mapping=scene250map, sep=',')
    elif sim_type == 'coco_rel':
        f = open('data/scene250_averaged.txt')
        lines = f.read().split('\n')
        return evaluate_correlation_csv_dataset(lines, vector_dictionary, similarity, start_line=0, proper_line_lenght=4, proper_n_pairs=250,
                                                positions=(0, 1, 3), mapping=scene250map, sep=',')
    elif sim_type == 'simlex999':
        f = open(os.path.join(SIMLEX_PATH, 'SimLex-999.txt'))
        lines = f.read().split('\n')
        return evaluate_correlation_csv_dataset(lines, vector_dictionary, similarity,
                                                positions=(0, 1, 3), proper_line_lenght=10, sep='\t')

    else:
        raise ValueError('Similarity type ' + sim_type + ' does not exist.')


def get_all_words():
    import functools

    wordsim_files = [
        (os.path.join(SIMLEX_PATH, 'SimLex-999.txt'), '\t'),
        (os.path.join('data/scene250_averaged.txt'), ','),
        (os.path.join(MEN_PATH, 'MEN_dataset_natural_form_full'), ' '),
    ]
    words = set()
    for filename, sep in wordsim_files:
        with open(filename) as f:
            d = f.read()
            words.update([w for w in functools.reduce(lambda a,b:a+b,[l.split(sep)[:2] for l in d.split('\n')]) if len(w)>0])
    return list(words)


def evaluate_nearest_neighbors_all_k(vector_dictionary, supercategories, similarity='euc'):

    if len(vector_dictionary) == 0:
        raise ValueError('Vector dictionary is empty')

    vectors = np.array(list(vector_dictionary.values()))
    vector_supercategories = np.array([supercategories[k] for k in vector_dictionary.keys()])

    print('using {} supercategories on {} words'.format(len(set(vector_supercategories)), len(vectors)))

    assert len(vectors) == len(vector_supercategories)

    if similarity == 'cos':
        # for cosine similarity, normalize vectors to magnitude 1, which leads to dot sim = cos sim
        magnitudes = np.sqrt((vectors ** 2).sum(1))
        vectors = vectors / magnitudes[:, np.newaxis]

        assert np.abs(np.sqrt((vectors ** 2).sum(1)) - np.ones(len(vectors))).mean() < 0.03, str(vectors)

    scores = []
    for vector, supercategory in zip(vectors, vector_supercategories):
        scores_per_k = []
        for k in range(1, 5):

            if similarity == 'euc':
                nearest_neighbors = np.sqrt(np.power(vectors - vector, 2).sum(1)).argsort()[1:k+1]
            elif similarity in {'dot', 'cos'}:
                nearest_neighbors = vectors.dot(vector).argsort()[::-1][1:k+1]
            else:
                raise ValueError('invalid similarity: '+similarity)

            neighbor_supercategories = [vector_supercategories[n] for n in nearest_neighbors]
            score = sum([c == supercategory for c in neighbor_supercategories]) / k
            scores_per_k += [score]

        scores += [np.mean(scores_per_k)]

    return np.mean(scores)


def evaluate_knn(vector_dictionary, supercategories, k=3, similarity='euc', cumulative=False):

    if len(vector_dictionary) == 0:
        raise ValueError('Vector dictionary is empty')

    idx = {w: i for i, w in enumerate(vector_dictionary.keys())}
    idx_inv = {i: w for w, i in list(idx.items())}

    vectors = np.array([vector_dictionary[idx_inv[i]] for i in list(idx_inv.keys())])

    # normalize for cosine distance
    vectors = vectors / np.sqrt((vectors**2).sum(1))[:, np.newaxis]
    distance_matrix = vectors.dot(vectors.T)
    neighbor_matrix = distance_matrix.argsort(1)[:, ::-1]

    precisions, recalls = [], []

    for this_supercategory in set(supercategories.values()):

        tp, fp, tn, fn = 0, 0, 0, 0
        for i in range(vectors.shape[0]):
            concept_super = supercategories[idx_inv[i]]

            neighbors = neighbor_matrix[i][1:k+1]
            neighbors_super = [supercategories[idx_inv[j]] for j in neighbors]
            neighbor_pred = mode(neighbors_super).mode[0]

            # print(concept_super, neighbor_pred, this_supercategory)

            if this_supercategory == concept_super:
                if neighbor_pred == concept_super:
                    tp += 1
                else:
                    fn += 1
            elif this_supercategory == neighbor_pred:  # prediction from neighbors is this_supercategory
                fp += 1
            else:
                tn += 1

        # print(tp, fp, tn, fn)

        precision = tp / (tp + fp) if tp != 0 else 0
        recall = tp / (tp + fn)
        fpr = fp / (fp + tn)

        # print(supercategory, 'precision', precision)
        # print(supercategory, 'recall', recall)

        precisions += [precision]
        recalls += [recall]

    return np.mean(precisions), np.mean(recalls)



if __name__ == '__main__':

    vectors = {
        'car': np.array([0, 1, 1, 0.2, 1]),
        'automobile': np.array([0.3, 1, 1, 0.2, 1]),
        'dog': np.array([0, 0.6, 1, 0, 0.7]),
        'cat': np.array([110, 0, 0.2, 0, 1]),
        'journey': np.array([0.1, 0, 0.7, 0, 97]),
        'bird': np.array([0.8, 0, 0.1, 0, 0.1]),
        'crane': np.array([0.9, 0, 0.7, 0.3, 1]),
        'rabbit': np.array([0.2, 0.1, 0.3, 0.1, .2]),
        'bull': np.array([0.1, 0.3, 0.1, 0.8, .2]),
        'lamb': np.array([0.2, 0.1, 0.1, 0.6, .1]),
        'calf': np.array([0.1, 0.5, 0.2, 0.2, .9]),
    }

    perfect_vectors = {
        'car': np.array([0, 0.9, 0.1, 0.2, 1]),
        'automobile': np.array([0, 0.8, 0.2, 0.1, 0.9]),
        'dog': np.array([0, 0.2, 0.8, 0.1, 0.7]),
        'cat': np.array([0, 0.6, 0.9, 0, 0.5]),
        'journey': np.array([0.2, 0, 0.6, 0, 0.7]),
        'bird': np.array([0, 0.3, 0.7, 0.1, 0.7]),
        'crane': np.array([0, 0.7, 0.2, 0.1, 1]),
    }

    supercategories = {
        'car': 'vehicle',
        'automobile': 'vehicle',
        'dog': 'animal',
        'cat': 'animal',
        'bird': 'animal',
        'journey': 'thing',
        'crane': 'vehicle',
    }

    print(4.0/3.0)
    print(evaluate_nearest_neighbors_all_k(perfect_vectors, supercategories, similarity='cos'))
    print(evaluate_nearest_neighbors_all_k(perfect_vectors, supercategories, similarity='dot'))
    print(evaluate_nearest_neighbors_all_k(perfect_vectors, supercategories, similarity='euc'))
