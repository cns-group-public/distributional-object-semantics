import time
from collections import Counter, OrderedDict
import numpy as np
from object_context.contexts import ContextBase


# definitions of the early fusion methods
def early_fusion_none(contexts):
    assert all(type(c) == np.ndarray for c in contexts)
    return np.hstack(contexts)


def early_fusion_concat(contexts):
    assert all(type(c) == np.ndarray for c in contexts)
    return np.hstack(contexts).astype('float32')


def early_fusion_norm(contexts):
    assert all(type(c) == np.ndarray for c in contexts)

    means = [c.mean() for c in contexts]
    stds = [c.std() + 0.000001 for c in contexts]
    return np.concatenate([(c - mean) / std for mean, std, c in zip(means, stds, contexts)]).astype('float32')


def early_fusion_equal(contexts):
    assert all(type(c) == np.ndarray for c in contexts)

    inv_lens = [1 / len(c) for c in contexts]
    return np.concatenate([c * inv_len for inv_len, c in zip(inv_lens, contexts)]).astype('float32')


def early_fusion_sum(contexts):
    assert all(type(c) == np.ndarray for c in contexts)
    return np.array(contexts).sum(0).astype('float32')


def early_fusion_average(contexts):
    assert all(type(c) == np.ndarray for c in contexts)
    return np.array(contexts).mean(0)


class Extract(object):
    """ Takes scene and context model and conducts the extraction. """

    change_zero_vectors = True

    def __init__(self, scene_model, context_types, sizes=(), fusion_method=None, load_from_disk=True):
        super().__init__()

        assert fusion_method in {'concat', 'norm', 'equal', 'average', 'sum', None}
        self.fusion = fusion_method

        self.scene_model = scene_model
        self.context_types = context_types
        assert all([issubclass(context_type, ContextBase) for context_type in context_types])

        # cache in case any of the context requires a cache
        self.cache = any([context_type.cache for context_type in context_types])
        # self.context_model = None
        self.sizes = sizes
        self.objects = None
        self.contexts = None
        self.load_from_disk = load_from_disk
        self.context_size = None
        self.obj2idx = None
        self.el2idx = None

        self.early_fusion_methods = {
            'concat': early_fusion_concat,
            'norm': early_fusion_norm,
            'equal': early_fusion_equal,
            'sum': early_fusion_sum,
            'average': early_fusion_average,
            None: early_fusion_none}

    def name(self):
        n = '{}_{}'.format(self.scene_model.name(), '+'.join(context_type.name
                                                             for context_type in self.context_types))
        n += '' if self.fusion is None else '_' + self.fusion
        return n

    def compute_occurrences(self, expand_concept=None):

        concept_counter = Counter()
        expansion_concepts = set()
        n_scenes = len(self.scene_model.get_scenes())

        time_stamp = time.time()

        # identify all object classes, i.e. compute obj2idx
        for i_scene, (_, items) in enumerate(self.scene_model.get_scenes()):

            for item in items:
                item_category = item[0]
                concept_counter.update([item_category])

                if expand_concept is not None:
                    expansion_concepts.update({expand_concept(item_category)})

            if i_scene % 500 == 0 and time.time() - time_stamp > 60:
                print('%.3f' % (100 * float(i_scene) / float(n_scenes)), end=' ')
                print('%')
                time_stamp = time.time()

        obj2idx = dict([(c, i) for i, (c, count) in enumerate(concept_counter.most_common())])

        expansion_categories = expansion_concepts.difference(obj2idx.keys())

        # copy mapping from obj2idx and add expanded categories at the end
        el2idx = obj2idx.copy()

        for i, c in enumerate(expansion_categories):
            el2idx[c] = len(obj2idx) + i

        return obj2idx, el2idx

    def extract(self):
        print('\nneed to extract contexts')

        if self.scene_model.scenes is None:
            print('initialize scene model', self.scene_model.name())
            self.scene_model.initialize()

        self.objects = None
        contexts = []
        context_size = 0
        self.obj2idx, self.el2idx = self.compute_occurrences()

        for context_type in self.context_types:

            print('extracting {} from {}, skip by ctrl+c. {} scenes'.format(
                context_type.__name__, self.scene_model.name(), len(self.scene_model.get_scenes())))

            context_model = context_type()

            t = time.time()
            # second pass: compute contexts

            proc_scenes = context_model.process_scenes(self.scene_model.get_scenes(), self.obj2idx, self.el2idx)
            extracted_objects, extracted_contexts, was_skipped = proc_scenes

            assert len(extracted_objects) == len(extracted_contexts)

            if was_skipped:
                print('skipped', len(self.objects))
                self.name += '_skipped@' + str(len(self.objects))

            extracted_objects = np.array(extracted_objects, dtype='uint16')

            assert type(extracted_contexts[0]) == np.ndarray
            assert extracted_contexts[0].ndim == 1

            # self.contexts = np.array(self.contexts, dtype='float32')

            context_size_set = set([len(c) for c in extracted_contexts])
            context_size += list(context_size_set)[0]
            assert len(context_size_set) == 1, 'contexts may not have different lengths'

            print('# of nan: {}\nruntime: {:.2}s\nfound {} objects in {} scenes'.format(
                context_model.nan_counter, np.round(time.time() - t), len(extracted_objects),
                len(self.scene_model.get_scenes())
            ))

            if self.objects is None:
                self.objects = extracted_objects
                contexts = [[c] for c in extracted_contexts]
            else:
                # import ipdb
                # ipdb.set_trace()

                # this is important. the order of objects must be consistent across context models, otherwise
                # the early fusion can not work.
                assert all([o1 == o2 for o1, o2 in zip(self.objects, extracted_objects)])
                assert len(contexts) == len(extracted_contexts) == len(self.objects)
                for i, c in enumerate(extracted_contexts):
                    contexts[i] += [c]

        # contexts should look like [[context_array1, optional_context_array2, ...], ...]

        assert type(contexts[0]) == list
        assert type(contexts[0][0]) == np.ndarray
        assert contexts[0][0].ndim == 1

        self.contexts = [self.early_fusion_methods[self.fusion](c) for c in contexts]

        assert type(self.contexts[0]) == np.ndarray
        assert self.contexts[0].ndim == 1
        self.context_size = len(self.contexts[0])


class TextContextExtractor(object):
    """ This is just for testing the Skip-gram implementation with text data. """

    def __init__(self, filename):
        import zipfile
        with zipfile.ZipFile(filename) as f:
            self.text = str(f.read(f.namelist()[0]))

        self.contexts = None
        self.cache = False

    def name(self):
        return 'TextContentExtractor'

    def extract(self):
        print('extract')

        words = self.text.split()
        n_classes = 50000

        most_common = Counter(words).most_common()

        self.obj2idx = OrderedDict([(w, i) for i, (w, _) in enumerate(most_common[:n_classes])])
        self.obj2idx.update(OrderedDict([(w, n_classes) for _, (w, _) in enumerate(most_common[n_classes:])]))


        self.objects = [self.obj2idx[words[i]] for i in range(1, len(words)-1)]
        self.contexts = [[self.obj2idx[words[i-1]], self.obj2idx[words[i+1]]] for i in range(1, len(words)-1)]

        assert len(self.objects) == len(self.contexts)


