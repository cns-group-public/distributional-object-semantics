import json
import sys

from configurations import rcnn_scenes, word2vec_vectors, coco_scenes
from object_context.concept_vectors import LateFusionNorm
from object_context.contexts import VGG16Context, CoOccurrenceContextNoself, CoOccurrenceContextSelf, \
    CoCountContextSelf, DummyContext
from object_context.extract import Extract
from object_context.integration import Averaging
from matplotlib import rcParams

from object_context.visualization import cluster_visualization, trait_visualization, text_visualization

rcParams['font.sans-serif'] = ['Lato', 'sans-serif']
rcParams['font.weight'] = 'black'
rcParams['font.size'] = '20'

traits = {
    'can_fly': ['bird', 'airplane', 'frisbee', 'kite'],
    'has_wheels': ['motorcycle', 'bicycle', 'bus', 'car', 'skateboard'],
    'sharp': ['scissors', 'knife', 'fork'],
    'round': ['pizza', 'sports ball', 'orange', 'apple', 'donut'],
    'larger_than_human': ['car', 'airplane', 'train', 'elephant', 'giraffe', 'zebra', 'bear', 'bed', 'couch'],
    'fragile': ['bottle', 'cup', 'vase'],
    'soft': ['sheep', 'broccoli', 'baseball glove', 'sandwich', 'backpack', 'cake', 'teddy bear', 'couch']
}

trait_configurations = [
    Averaging(Extract(rcnn_scenes, [VGG16Context])),
    Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself])),
    word2vec_vectors,
]

general_configurations = [
    word2vec_vectors,
    LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])),
                   Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
    Averaging(Extract(coco_scenes, [CoOccurrenceContextSelf])),
    Averaging(Extract(rcnn_scenes, [VGG16Context])),
    Averaging(Extract(coco_scenes, [CoCountContextSelf])),
    Averaging(Extract(coco_scenes, [DummyContext])),
]

supercategories = json.load(open('data/supercategories.json'))
supercategories_list = list(set(supercategories.values()))
supercategory_map = {k: supercategories_list.index(v) for k, v in list(supercategories.items())}

coco_classes = list(supercategories.keys())

if __name__ == '__main__':

    arg = sys.argv[1]

    if arg == 'general':
        for conf in general_configurations:
            cluster_visualization(conf, supercategory_map, distance='cos', filename=conf.name() + '_cluster.pdf')
            text_visualization(conf, supercategory_map, distance='cos', filename=conf.name() + '_text.pdf')

    if arg == 'traits':
        for conf in trait_configurations:
            trait_visualization(conf, traits, coco_classes)

