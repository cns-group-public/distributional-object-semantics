#!/usr/bin/python3
from copy import deepcopy
from logging import warning
import matplotlib

matplotlib.use('Agg')

import os
import json
import itertools
import pylab
import sys
from collections import OrderedDict
from object_context.evaluate import evaluate_correlation, evaluate_nearest_neighbors_all_k, evaluate_knn
from configurations import experiments

supercategories = json.load(open('data/supercategories.json'))

supercategories_lm = deepcopy(supercategories)
supercategories_lm.update(json.load(open('data/supercategories_lm.json')))

if not os.path.exists('figures'):
    os.makedirs('figures')

pylab.rcParams['xtick.labelsize'] = 15
pylab.rcParams['ytick.labelsize'] = 15
markers = "o+x*sdhv8>"

repetitions = 1
fraction = 1.0
plot_sizes = [100, 250, 1000, 5000, 10000, 50000, 250000, 700000]
valid_labelme_objects = json.load(open('data/labelme_count3_trunc.json'))

supercategories_list = list(set(supercategories.values()))
supercategory_map = {k: supercategories_list.index(v) for k, v in list(supercategories.items())}


def check_experiments_validity(experiments):
    lengths = [len(e) for e in list(experiments.values())]
    if not all([l == 4 for l in lengths]):
        raise ValueError('Some experiment tuples have wrong length (should be 6): '+ str(lengths))


if __name__ == '__main__':

    check_experiments_validity(experiments)

    if len(sys.argv) > 1:
        arg = sys.argv[1]
        arg = arg.split(',')
    else:
        arg = None

    if arg is not None:
        if all([e in experiments for e in arg]):
            experiments = {e: experiments[e] for e in arg}
        else:
            raise ValueError('some experiments were not found.')

    for experiments_name, experiment_config in list(experiments.items()):

        experiments_scenes_context, n_contexts, similarities, sim_datasets = experiment_config
        print('\n\n\nExperiment:', experiments_name)

        eval_nn = OrderedDict()
        eval_precision = OrderedDict()
        eval_recall = OrderedDict()
        eval_sizes = OrderedDict()

        # determine common concepts across this experiment (i.e. supported by all embeddings).
        common_concepts = set(experiments_scenes_context[0].get_words())
        for concept_vectors in experiments_scenes_context[1:]:
            print(len(common_concepts), concept_vectors.name())
            common_concepts = common_concepts.intersection(list(concept_vectors.get_words()))

        print(len(common_concepts), 'common concept classes were found')
        assert len(common_concepts) > 0

        for i_experiment, concept_vectors in enumerate(experiments_scenes_context):

            experiment_id = concept_vectors.name()
            concept_vectors.reduce_concepts_to(common_concepts)

            print('\n\nnext setting:', experiments_name, experiment_id)

            eval_nn[experiment_id] = {d + '_' + s: [] for s in similarities for d in sim_datasets}
            eval_nn[experiment_id].update({s: [] for s in similarities})
            eval_nn[experiment_id].update({s+'_lm': [] for s in similarities})
            eval_precision[experiment_id] = {s: [] for s in similarities}
            eval_recall[experiment_id] = {s: [] for s in similarities}
            # eval_sim[experiment_id] = {dataset + '_' + s: [] for s in similarities for dataset in sim_datasets}

            # repetitions only make sense if the integration method is not deterministic
            for j in range(concept_vectors.repetitions):
                # only use words for nearest neighbors that are shared across all embeddings in the experiment
                v_supercategories = concept_vectors.get_common(common_concepts.intersection(list(supercategories.keys())))
                v_supercategories_lm = concept_vectors.get_common(common_concepts.intersection(list(supercategories_lm.keys())))

                if j != 0:  # no need to recompute in the first round
                    concept_vectors.integrate()

                for s in similarities:

                    p, r = evaluate_knn(v_supercategories, supercategories, similarity=s)
                    eval_precision[experiment_id][s] += [p]
                    eval_recall[experiment_id][s] += [r]

                    r = evaluate_nearest_neighbors_all_k(v_supercategories, supercategories, similarity=s)
                    eval_nn[experiment_id][s] += [r]

                    r = evaluate_nearest_neighbors_all_k(v_supercategories_lm, supercategories_lm, similarity=s)
                    eval_nn[experiment_id][s+'_lm'] += [r]

                    for sim_dataset in sim_datasets:
                        r = evaluate_correlation(sim_dataset, concept_vectors.vectors, s)
                        eval_nn[experiment_id][sim_dataset + '_' + s] += [r]

            if len(n_contexts) > 0:
                eval_sizes[experiment_id] = {s: [] for s in similarities}
                eval_sizes[experiment_id].update({ws + '_' + s: [] for s, ws in itertools.product(similarities, sim_datasets)})

            for n_context_elements in n_contexts:
                concept_vectors.integrate(n_contexts=n_context_elements)
                v_supercategories = concept_vectors.get_common(common_concepts.intersection(list(supercategories.keys())))
                print('size {} running the neighborhood consistency benchmark on {} concept classes'.format(
                    n_context_elements, len(v_supercategories)))

                for s in similarities:
                    eval_sizes[experiment_id][s] += [evaluate_nearest_neighbors_all_k(v_supercategories,
                                                                                      supercategories, similarity=s)]

                v_common = concept_vectors.get_common(common_concepts)

                if len(v_common) > 10:
                    print('size {} running the correlation benchmark on {} concept classes'.format(
                        n_context_elements, len(v_common)))
                    for s in similarities:
                        for sim_dataset in sim_datasets:
                            print('\nexperiment id', experiment_id)
                            r = evaluate_correlation(sim_dataset, v_common, s)
                            eval_sizes[experiment_id][sim_dataset + '_' + s] += [r]
                else:
                    warning('can not run correlation benchmark due to insufficient number of pairs.')

            concept_vectors.free()

        # save the experiment's data to allow re-generation of diagrams without re-running
        json.dump(list(common_concepts), open('figures/' + experiments_name + '_common_concepts.json', 'w'))
        json.dump(eval_sizes, open('figures/' + experiments_name + '_eval_sizes.json', 'w'))
        json.dump(eval_nn, open('figures/' + experiments_name + '_eval_nn.json', 'w'))
        json.dump(eval_precision, open('figures/' + experiments_name + '_eval_precision.json', 'w'))
        json.dump(eval_recall, open('figures/' + experiments_name + '_eval_recall.json', 'w'))

        print('experiment done\n\n')
