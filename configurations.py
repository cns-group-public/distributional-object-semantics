import json

from object_context.integration import Averaging, SkipGram, Median
from object_context.extract import Extract
# from object_context.context_by_annotations import *
from object_context.contexts import VGG16Context, CoCountContextNoself, CoCountContextSelf, \
    CoOccurrenceContextNoself, CoOccurrenceContextSelf, DummyContext, ResNet50Context, InceptionV1Context, \
    InceptionV4Context
from object_context.evaluate import get_all_words

from object_context.scenes import LabelMeScenes, MSCOCOScenes, RCNNAllScenes
from object_context.concept_vectors import Word2VecVectors, KielaMaxVectors, KielaMeanVectors, GloveVectors, \
    LateFusionNorm, LateFusionEqual, Random, LateFusionNormMeanMag, LateFusionNormToOne, LateFusionNormStdOnly

#mapping = json.load(open('data/coco_class2synset.json'))
#mapping = json.load(open('synset_sim.json'))
#mapping = None

# for coco classes we can always use synsets because the mapping is 1-to-1
#coco_scenes_syn = MSCOCOScenes(mapping, None)

MAX_SCENES = None

# load unmapped labelme
labelme_scenes = LabelMeScenes(MAX_SCENES, None, True, False)
# labelme_scenes.initialize()

coco_scenes = MSCOCOScenes(None, MAX_SCENES)
coco_scenes_small = MSCOCOScenes(None, 500)
# coco_scenes.initialize()
# rcnn_scenes = RCNNScenes('/media/resources/MS_COCO/train2014', 30000, 0.97)
#rcnn_scenes.apply(mapping)

# map to synsets
# labelme_scenes.apply_or_remove_item(json.load(open('data/labelme_count3_trunc.json')))
# labelme_scenes_syn = copy(labelme_scenes)
# labelme_scenes_syn.apply_or_remove_item(json.load(open('data/labelme_label2synset.json')))

# print 'labelme objects', len(labelme_scenes.all_occurring_classes())
#rcnn_scenes = coco_scenes
#labelme_scenes = coco_scenes

#rcnn_scenes = RCNNScenes(5, '/media/resources/MS_COCO/train2014')

# print 'mscoco objects', len(coco_scenes.all_occurring_classes())

#ContextAverage(labelme_scenes, BVLCNetContext()).get()
#ContextAverage(labelme_scenes, ResNet50Context()).get()
#ContextAverage(labelme_scenes, DummyContext(1))


selected_words_labelme_syn = ['motorcycle.n.01', 'train.n.01', 'airplane.n.01', 'car.n.01', 'bicycle.n.01',
                              # 'apple.n.01', 'banana.n.01', 'fork.n.01',
                              'knife.n.01', 'spoon.n.01', 'bowl.n.01']  # 'pizza.n.01',  'cake.n.01']

selected_words_labelme = ['motorcycle', 'train', 'airplane', 'car', 'bicycle',
                          # 'apple', 'banana', 'fork',
                          'knife', 'spoon', 'bowl',  # 'pizza',
                          'cake']

selected_words_mscoco_syn = ['motorcycle.n.01', 'train.n.01', 'airplane.n.01', 'car.n.01', 'bicycle.n.01',
                             'apple.n.01', 'banana.n.01', 'fork.n.01',
                             'knife.n.01', 'spoon.n.01', 'bowl.n.01',  # 'pizza.n.01',
                             'cake.n.01']

selected_words_mscoco = ['motorcycle', 'train', 'airplane', 'car', 'bicycle',
                         'apple', 'banana', 'fork', 'knife', 'spoon', 'bowl']  # 'pizza','cake']

n_contexts_ms = [100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 500000, 604907]
n_contexts_lm = [100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 325288]
n_contexts_rn = [100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 286835]

#
# sizes = [100, 250, 500, 1000, 5000]
# sizes_ms = sizes
# sizes_lm = sizes

try:
    all_words = json.load(open('data/all_words.json'))
except FileNotFoundError:
    labelme_scenes.initialize()
    coco_scenes.initialize()
    all_words = list(set(get_all_words() + labelme_scenes.all_occurring_objects() + coco_scenes.all_occurring_objects()))
    print('number of occurring words', len(all_words))
    json.dump(all_words, open('data/all_words.json', 'w'))

print(all_words)
coco_classes = json.load(open('data/classes.json'))
print(set(all_words).intersection(coco_classes))


word2vec_vectors = Word2VecVectors('GoogleNews-vectors-negative300.bin', words=all_words)
glove_vectors = GloveVectors(words=all_words)

rcnn_scenes = RCNNAllScenes(160000)
# rcnn_scenes.initialize()

# coco_scenes = coco_scenes_small
# n_contexts_ms = [100, 250, 500, 1000]

experiments = {
    #name: ([VectorSpace, VectorSpace], [sizes], [hitrates], [metrics], [wordsim datasets], [mapping to synset for wordsim datasets])
    #'mscoco': ([[a1], [a1, a2]], [], [3, 10], ['cos'], ['cocosim'], mapping, coco_common),


    # 'problems': ([
    #     [ContextAverage(coco_scenes, InceptionV1Context()).set_change_zero_vectors()],
    # ], [], [3, 10], ['cos', 'euc'], ['coco_sim', 'coco_rel'], selected_words_mscoco),

    # comparison for each scene type isolated

    'test': ([
         Averaging(Extract(labelme_scenes, [InceptionV1Context])),
         Averaging(Extract(labelme_scenes, [VGG16Context])),
         Random(labelme_scenes),
    ], [], ['cos', 'euc'], ['coco_sim', 'coco_rel']),

    'mscoco': ([
         Averaging(Extract(coco_scenes, [InceptionV1Context])),
         Averaging(Extract(coco_scenes, [InceptionV4Context])),
         Averaging(Extract(coco_scenes, [VGG16Context])),
         Averaging(Extract(coco_scenes, [ResNet50Context])),
         Averaging(Extract(coco_scenes, [CoCountContextNoself])),
         Averaging(Extract(coco_scenes, [CoCountContextSelf])),
         Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself])),
         Averaging(Extract(coco_scenes, [CoOccurrenceContextSelf])),

         SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself])),

         Random(coco_scenes),
         # Averaging(Extract(coco_scenes, [DummyContext])),
    ], n_contexts_ms, ['cos', 'euc'], ['coco_sim', 'coco_rel']),

    'rcnn': ([
        Averaging(Extract(rcnn_scenes, [InceptionV1Context])),
        Averaging(Extract(rcnn_scenes, [InceptionV4Context])),
        Averaging(Extract(rcnn_scenes, [VGG16Context])),
        Averaging(Extract(rcnn_scenes, [ResNet50Context])),
        Averaging(Extract(rcnn_scenes, [CoCountContextNoself])),
        Averaging(Extract(rcnn_scenes, [CoCountContextSelf])),
        Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(rcnn_scenes, [CoOccurrenceContextSelf])),

        SkipGram(Extract(rcnn_scenes, [CoOccurrenceContextNoself])),
        #Averaging(Extract(rcnn_scenes, [DummyContext])),
        Random(rcnn_scenes),
    ], n_contexts_rn, ['cos', 'euc'], ['coco_sim', 'coco_rel']),

    # comparison for each scene type isolated
    'labelme': ([
        Averaging(Extract(labelme_scenes, [InceptionV1Context])),
        Averaging(Extract(labelme_scenes, [InceptionV4Context])),
        Averaging(Extract(labelme_scenes, [VGG16Context])),
        Averaging(Extract(labelme_scenes, [ResNet50Context])),
        Averaging(Extract(labelme_scenes, [CoCountContextNoself])),
        Averaging(Extract(labelme_scenes, [CoCountContextSelf])),
        Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(labelme_scenes, [CoOccurrenceContextSelf])),

        SkipGram(Extract(labelme_scenes, [CoOccurrenceContextNoself])),
        #Averaging(Extract(labelme_scenes, [DummyContext])),
        Random(labelme_scenes),
    ], n_contexts_lm, ['cos', 'euc'], ['coco_sim', 'coco_rel']),

    # # ------------------------------------------
    'fusion_mscoco': ([

        # early fusion

        # Averaging(Extract(coco_scenes, [VGG16Context, CoOccurrenceContextSelf])),

        # Averaging(Extract(coco_scenes, [InceptionV1Context, CoOccurrenceContextSelf])),
        Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself])),
        # # late fusion
        Averaging(Extract(coco_scenes, [VGG16Context])),
        Averaging(Extract(coco_scenes, [ResNet50Context])),

        LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormStdOnly(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormMeanMag(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormToOne(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        Averaging(Extract(coco_scenes, [VGG16Context, CoOccurrenceContextNoself], fusion_method='norm')),
        # LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextSelf]))),
        # LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoCountContextNoself]))),
        # LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoCountContextSelf]))),


        LateFusionNorm(Averaging(Extract(coco_scenes, [ResNet50Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormStdOnly(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormMeanMag(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormToOne(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        Averaging(Extract(coco_scenes, [ResNet50Context, CoOccurrenceContextNoself], fusion_method='norm')),
        # LateFusionNorm(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextSelf]))),
        # LateFusionNorm(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoCountContextNoself]))),
        # LateFusionNorm(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoCountContextSelf]))),

    ], [], ['cos'], ['coco_sim', 'coco_rel']),


    'fusion_rcnn': ([
        # early fusion
        # Averaging(Extract(rcnn_scenes, [VGG16Context, CoOccurrenceContextNoself])),
        # Averaging(Extract(rcnn_scenes, [VGG16Context, CoOccurrenceContextSelf])),
        # Averaging(Extract(rcnn_scenes, [InceptionV1Context, CoOccurrenceContextNoself])),
        # Averaging(Extract(rcnn_scenes, [InceptionV1Context, CoOccurrenceContextSelf])),

        Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(rcnn_scenes, [VGG16Context])),
        Averaging(Extract(rcnn_scenes, [ResNet50Context])),

        LateFusionNorm(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormStdOnly(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormMeanMag(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormToOne(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        Averaging(Extract(rcnn_scenes, [VGG16Context, CoOccurrenceContextNoself], fusion_method='norm')),
        # LateFusionNorm(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextSelf]))),
        # LateFusionNorm(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoCountContextNoself]))),
        # LateFusionNorm(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoCountContextSelf]))),

        LateFusionNorm(Averaging(Extract(rcnn_scenes, [ResNet50Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormStdOnly(Averaging(Extract(rcnn_scenes, [InceptionV1Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormMeanMag(Averaging(Extract(rcnn_scenes, [InceptionV1Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        #LateFusionNormToOne(Averaging(Extract(rcnn_scenes, [InceptionV1Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        Averaging(Extract(rcnn_scenes, [ResNet50Context, CoOccurrenceContextNoself], fusion_method='norm')),


        # TODO: cross dataset fusion

        # LateFusionNorm(Averaging(Extract(rcnn_scenes, [InceptionV1Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextSelf]))),
        # LateFusionNorm(Averaging(Extract(rcnn_scenes, [InceptionV1Context])), Averaging(Extract(rcnn_scenes, [CoCountContextNoself]))),
         # LateFusionNorm(Averaging(Extract(rcnn_scenes, [InceptionV1Context])), Averaging(Extract(rcnn_scenes, [CoCountContextSelf]))),
    ], [], ['cos'], ['coco_sim', 'coco_rel']),

    # # ------------------------------------------
    'sota_rcnn': ([

        # unsupervised
        Averaging(Extract(rcnn_scenes, [ResNet50Context])),
        LateFusionNorm(Averaging(Extract(rcnn_scenes, [ResNet50Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        # LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), glove_vectors),
        LateFusionNorm(Averaging(Extract(rcnn_scenes, [ResNet50Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself])), word2vec_vectors),


        Averaging(Extract(rcnn_scenes, [VGG16Context])),
        LateFusionNorm(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself]))),
        # LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), glove_vectors),
        LateFusionNorm(Averaging(Extract(rcnn_scenes, [VGG16Context])), Averaging(Extract(rcnn_scenes, [CoOccurrenceContextNoself])), word2vec_vectors),
        glove_vectors,
        word2vec_vectors,
        KielaMaxVectors('/media/resources/KielaMultimodalEmbedding/embeddings/vectors', words=all_words),
        KielaMeanVectors('/media/resources/KielaMultimodalEmbedding/embeddings/vectors', words=all_words),
    ], [], ['cos'], ['coco_sim', 'coco_rel', 'MEN', 'simlex999']),


    'sota_labelme': ([
        # unsupervised: not possible because of 80 categories in RCNN

        # supervised:
        # Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(labelme_scenes, [ResNet50Context])),
        LateFusionNorm(Averaging(Extract(labelme_scenes, [ResNet50Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself]))),
        # LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), glove_vectors),
        LateFusionNorm(Averaging(Extract(labelme_scenes, [ResNet50Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), word2vec_vectors),
        #
        #
        Averaging(Extract(labelme_scenes, [VGG16Context])),
        LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself]))),
        # LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), glove_vectors),
        LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), word2vec_vectors),

        glove_vectors,
        word2vec_vectors,
        KielaMaxVectors('/media/resources/KielaMultimodalEmbedding/embeddings/vectors', words=all_words),
        KielaMeanVectors('/media/resources/KielaMultimodalEmbedding/embeddings/vectors', words=all_words)
    ], [], ['cos'], ['coco_sim', 'coco_rel', 'MEN', 'simlex999']),


    'sota_mscoco': ([
        # unsupervised: not possible because of 80 categories in RCNN

        # supervised:
        # Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(coco_scenes, [ResNet50Context])),
        LateFusionNorm(Averaging(Extract(coco_scenes, [ResNet50Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself]))),
        # LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), glove_vectors),
        LateFusionNorm(Averaging(Extract(coco_scenes, [ResNet50Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), word2vec_vectors),
        #
        #
        Averaging(Extract(coco_scenes, [VGG16Context])),
        LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself]))),
        # LateFusionNorm(Averaging(Extract(labelme_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), glove_vectors),
        LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])), word2vec_vectors),

        glove_vectors,
        word2vec_vectors,
        KielaMaxVectors('/media/resources/KielaMultimodalEmbedding/embeddings/vectors', words=all_words),
        KielaMeanVectors('/media/resources/KielaMultimodalEmbedding/embeddings/vectors', words=all_words)
    ], [], ['cos'], ['coco_sim', 'coco_rel', 'MEN', 'simlex999']),


    # NEW STUFF
    # # ------------------------------------------
    'fusion_methods': ([

        # same size contexts
        Averaging(Extract(coco_scenes, [CoCountContextNoself])),
        Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(coco_scenes, [CoCountContextNoself, CoOccurrenceContextNoself], fusion_method='concat')),
        Averaging(Extract(coco_scenes, [CoCountContextNoself, CoOccurrenceContextNoself], fusion_method='sum')),
        Averaging(Extract(coco_scenes, [CoCountContextNoself, CoOccurrenceContextNoself], fusion_method='average')),

        # early fusion
        Averaging(Extract(coco_scenes, [InceptionV1Context, CoOccurrenceContextNoself], fusion_method='concat')),
        Averaging(Extract(coco_scenes, [InceptionV1Context, CoOccurrenceContextNoself], fusion_method='equal')),
        Averaging(Extract(coco_scenes, [InceptionV1Context, CoOccurrenceContextNoself], fusion_method='norm')),
        Averaging(Extract(coco_scenes, [VGG16Context, CoOccurrenceContextNoself], fusion_method='concat')),
        Averaging(Extract(coco_scenes, [VGG16Context, CoOccurrenceContextNoself], fusion_method='equal')),
        Averaging(Extract(coco_scenes, [VGG16Context, CoOccurrenceContextNoself], fusion_method='norm')),

        # late fusion
        LateFusionNorm(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        LateFusionEqual(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        LateFusionNormToOne(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        LateFusionNormStdOnly(Averaging(Extract(coco_scenes, [InceptionV1Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),

        LateFusionNorm(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        LateFusionEqual(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        LateFusionNormToOne(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
        LateFusionNormStdOnly(Averaging(Extract(coco_scenes, [VGG16Context])), Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself]))),
    ], [], ['cos'], ['coco_sim', 'coco_rel']),


    'fusion_methods_debug': ([
        # TODO: why is early fusion equal != late fusion equal?
        # same size contexts
        Averaging(Extract(coco_scenes, [CoCountContextNoself, CoOccurrenceContextNoself], fusion_method='equal')),
        LateFusionEqual(Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself])),
                        Averaging(Extract(coco_scenes, [CoCountContextNoself])))

    ], [], ['cos'], ['coco_sim', 'coco_rel']),

    'alternative_integration_mscoco': ([    
        Averaging(Extract(coco_scenes, [VGG16Context])),
        Averaging(Extract(coco_scenes, [ResNet50Context])),
        Averaging(Extract(coco_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(coco_scenes, [CoCountContextNoself])),

        Median(Extract(coco_scenes, [VGG16Context])),
        Median(Extract(coco_scenes, [ResNet50Context])),
        Median(Extract(coco_scenes, [CoOccurrenceContextNoself])),
        Median(Extract(coco_scenes, [CoCountContextNoself])),

    ], [], ['cos'], ['coco_sim', 'coco_rel']),
    
    'alternative_integration_labelme': ([    
        Averaging(Extract(labelme_scenes, [VGG16Context])),
        Averaging(Extract(labelme_scenes, [ResNet50Context])),
        Averaging(Extract(labelme_scenes, [CoOccurrenceContextNoself])),
        Averaging(Extract(labelme_scenes, [CoCountContextNoself])),

        Median(Extract(labelme_scenes, [VGG16Context])),
        Median(Extract(labelme_scenes, [ResNet50Context])),
        Median(Extract(labelme_scenes, [CoOccurrenceContextNoself])),
        Median(Extract(labelme_scenes, [CoCountContextNoself])),

    ], [], ['cos'], ['coco_sim', 'coco_rel']),

    'skip_gram': ([
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), n_negatives=10),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), n_negatives=25),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), n_negatives=50),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), epochs=10),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), epochs=50),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), epochs=100),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), embedding_size=64),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), embedding_size=128),
        SkipGram(Extract(coco_scenes, [CoOccurrenceContextNoself]), embedding_size=256),
    ], [], ['cos', 'euc'], ['coco_sim', 'coco_rel']),


}
