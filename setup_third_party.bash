#!/bin/bash

# get Mask_RCNN and apply small patch
git clone https://github.com/matterport/Mask_RCNN.git
cd Mask_RCNN
git checkout 555126ee899a144ceff09e90b5b2cf46c321200c
git apply ../mask-rcnn.patch
cd ..

# checkout the tensorflow models and pre-trained weights
mkdir nets
mkdir nets/preprocessing
mkdir models

models=( inception_v1.py inception_v4.py vgg.py resnet_v2.py inception_utils.py resnet_utils.py )
commit="376dc8dd0999e6333514bcb8a6beef2b5b1bb8da"

for m in "${models[@]}"; do
    wget https://raw.githubusercontent.com/tensorflow/models/$commit/research/slim/nets/$m -O nets/$m
done

wget https://raw.githubusercontent.com/tensorflow/models/$commit/research/slim/preprocessing/preprocessing_factory.py -O nets/preprocessing/preprocessing_factory.py
wget https://raw.githubusercontent.com/tensorflow/models/$commit/research/slim/preprocessing/vgg_preprocessing.py -O nets/preprocessing/vgg_preprocessing.py
wget https://raw.githubusercontent.com/tensorflow/models/$commit/research/slim/preprocessing/inception_preprocessing.py -O nets/preprocessing/inception_preprocessing.py

if [ "$1" == "with-weights" ]
then
    files=( inception_v1_2016_08_28.tar.gz inception_v4_2016_09_09.tar.gz vgg_16_2016_08_28.tar.gz resnet_v2_50_2017_04_14.tar.gz )
    for i in "${files[@]}"; do
       echo $i
       wget http://download.tensorflow.org/models/$i
       tar -xf $i -C models
       rm $i
    done
fi

